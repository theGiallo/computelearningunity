Shader "tg/sdf/Distance"
{
	Properties
	{
		_MainTex ( "Texture", 2D ) = "white" {}
		_ColorZero ( "Color Zero", Color ) = ( 0,0,0,1 )
		_ColorOne  ( "Color One",  Color ) = ( 1,1,1,1 )
	}
		SubShader
		{
			// No culling or depth
			Cull Off ZWrite Off ZTest Always ZClip False

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert ( appdata v )
				{
					v2f o;
					o.vertex = UnityObjectToClipPos( v.vertex );
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				fixed4 _ColorZero;
				fixed4 _ColorOne;

				float radius;

				fixed4 frag ( v2f i ) : SV_Target
				{
					fixed4 col;// = tex2D(_MainTex, i.uv);
					float depth = SAMPLE_DEPTH_TEXTURE( _MainTex, i.uv );
					//#if defined(UNITY_REVERSED_Z)
					//depth = 1 - depth;
					//#endif
					col = lerp( _ColorZero, _ColorOne, depth );
					return col;
				}
				ENDCG
			}
		}
}
