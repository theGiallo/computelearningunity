Shader "Hidden/Gray-ScottTextureWrite"
{
    Properties
    {
        _MainTex   ("Previous"  , 2D     ) = "white" {}
        _WriteTex  ("New State" , 2D     ) = "white" {}
        dt         ("dt"        , Float  ) = 1
        fk_min_max ("fk_min_max", Vector ) = (0.03,0.04,0.08,0.08)
        DA         ("DA"        , Float  ) = 1.0
        DB         ("DB"        , Float  ) = 0.5
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 5.0
            //#pragma require randomwrite
            #pragma require uav

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            Texture2D<float2> _MainTex;
            float4 _MainTex_TexelSize;
            RWTexture2D<float2> _WriteTex : register(u3);
            float dt ;//= 1;
            float4 fk_min_max;
            float DA ;//= 1.0;
            float DB ;//= 0.5;

            #define PIXEL(xy) _MainTex.Load( int3(xy,0) )

            float2 pixel( int2 xy, uint2 size )
            {
                xy = ( xy + size ) % size;
                //if ( xy.x < 0 ) xy.x += size.x;
                //if ( xy.y < 0 ) xy.y += size.y;
                //if ( xy.x >= size.x ) xy.x -= size.x;
                //if ( xy.y >= size.y ) xy.y -= size.y;
                return PIXEL( xy );
            }


            float4 frag (v2f i) : SV_Target
            {
                uint2 size = uint2(_MainTex_TexelSize.zw);
                int2 cell_coords = i.uv * size;

                float2 fk_min = fk_min_max.xy;
                float2 fk_max = fk_min_max.zw;
                float2 fk = fk_min + i.uv * ( fk_max - fk_min );

                float2 v3 = PIXEL( cell_coords );

                float2 n0 = pixel( cell_coords + int2( 1, 0), size );
                float2 n1 = pixel( cell_coords + int2( 1, 1), size );
                float2 n2 = pixel( cell_coords + int2( 0, 1), size );
                float2 n3 = pixel( cell_coords + int2(-1, 1), size );
                float2 n4 = pixel( cell_coords + int2(-1, 0), size );
                float2 n5 = pixel( cell_coords + int2(-1,-1), size );
                float2 n6 = pixel( cell_coords + int2( 0,-1), size );
                float2 n7 = pixel( cell_coords + int2( 1,-1), size );

                float2 acc  = 0;

                acc += n0;
                acc += n1;
                acc += n2;
                acc += n3;
                acc += n4;
                acc += n5;
                acc += n6;
                acc += n7;

                float2 lap = ( n0 + n2 + n4 + n6 ) * 0.2 + ( n1 + n3 + n5 + n7) * 0.05 - v3;

                float4 output = float4(v3,0,1);
                float A = v3.r;
                float B = v3.g;
                float lapA = lap.r;
                float lapB = lap.g;
                float f = fk.x;
                float k = fk.y;
                output.r = clamp( A + ( DA * lapA - A * B * B + f         * ( 1 - A ) ) * dt, 0, 1 );
                output.g = clamp( B + ( DB * lapB + A * B * B - ( k + f ) * B         ) * dt, 0, 1 );
                output.b = 0;

                _WriteTex[cell_coords] = output.rg;
                //discard;
                return output;
            }
            ENDCG
        }
    }
}
