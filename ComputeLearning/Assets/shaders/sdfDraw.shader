Shader "Hidden/sdfDraw"
{
	//Properties
	//{
	//	_PosBuf   ( "Vertex Position", 2D ) = "white" {}
	//	_DistBuf  ( "Vertex Distance", 2D ) = "white" {}
	//	_IndexBuf ( "Indices"        , 2D ) = "white" {}
	//}
	SubShader
	{
		Cull Off
		ZWrite On ZClip True ZTest Less

		// NOTE(theGiallo): not supported in some GPUs
		//Conservative True

		//Blend One One
		//BlendOp Min

		// TODO(theGiallo): THIS IS FOR DEBUG, USE THE ABOVE ONE
		// No culling or depth
		//Cull Off ZClip False ZWrite Off ZTest Always

		Pass
		{
			Cull Off
			ZWrite On ZClip True ZTest Less

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0
			#pragma require randomwrite
			#pragma require uav
			#pragma require compute
			#pragma multi_compile_local POLYLINE SEGMENTS
			#pragma multi_compile_local COMPUTE_POS_DIST __
			#pragma multi_compile_local SEGMENTS_IN_TEXTURE __
			#pragma multi_compile_local TWO_TRIS_PER_SEGMENT __
			#pragma multi_compile_local VERTS_GEN_CPU __
			#pragma multi_compile_local INDIRECT_RENDERING __

			#include "UnityCG.cginc"

			//#include "../compute/SegmentsToTris.cginc"

			#if 1//SHADER_API_MOBILE
			#define WPOSDIST_TEXTURE 1
			#endif
			uniform float radius;


			struct v2f
			{
				float4 vertex        : SV_POSITION;
				#if TWO_TRIS_PER_SEGMENT
				float2 ss_vertex     : TEXCOORD0;
				float  ab_len        : TEXCOORD1;
				#else
				float  dist          : TEXCOORD0;
				float  closest_index : TEXCOORD1;
				float2 ws_vertex     : TEXCOORD2;
				#endif
			};

			#if TWO_TRIS_PER_SEGMENT
				#define VERTICES_PER_SEGMENT 4
			#else
				#define VERTICES_PER_SEGMENT 12
			#endif

			#if ! VERTS_GEN_CPU || ! TWO_TRIS_PER_SEGMENT

				#if COMPUTE_POS_DIST

					uniform uint   segments_count;
					#if SEGMENTS_IN_TEXTURE
					uniform Texture2D<float2> read_segments_tex : register(t0); // (start(xy), end(xy))
					uniform uint read_segments_tex_width;
					#else
					uniform Buffer   <float2> read_segments     : register(t0); // (start(xy), end(xy))
					#endif

				#else

					#if 1//SHADER_API_MOBILE
					#define ENCODED_POS 1
					#endif


					#if WPOSDIST_F4
					uniform StructuredBuffer<float4> _PosDistBuf : register(t0);
					#elif WPOSDIST_TEXTURE
					uniform Texture2D<float4> _PosDistBuf : register(t0);
					#else
					uniform StructuredBuffer<float > _PosDistBuf : register(t0);
					#endif


				#endif

				uniform StructuredBuffer<uint  > _IndexBuf   : register(t1);
				#if ENABLE_DEBUG_BUF
				uniform RWStructuredBuffer<float4> _DebugVertexBuf : register(u2);
				#endif

				#if ENCODED_POS
				uniform float4x4 pos_decode_matrix;
				#endif


				#if COMPUTE_POS_DIST
					float2 read_segment( uint segment_index, uint ab01 )
					{
						#if POLYLINE
						uint i = segment_index + ab01;
						#else
						uint i = segment_index * 2 + ab01;
						#endif

						#if SEGMENTS_IN_TEXTURE
						uint2 i2;
						uint w = (uint)read_segments_tex_width;
						i2.y = i / w;
						i2.x = i - i2.y * w;
						//i2.y = 1023 - i2.y;
						float2 ret = read_segments_tex.Load(  uint3(i2,0) );
						#else
						float2 ret = read_segments    [i];
						#endif

						return ret;
					}

					#if TWO_TRIS_PER_SEGMENT
					bool compute_pos_dist( uint index, out float2 pos, out float2 rel_pos, out float ab_len )
					#else
					bool compute_pos_dist( uint index, out float2 pos, out float dist )
					#endif
					{
						#if TWO_TRIS_PER_SEGMENT
							#define VERTS_PER_SEGMENT 4
						#else
							#define VERTS_PER_SEGMENT 12
						#endif

						uint segment_index = index / VERTS_PER_SEGMENT;
						if ( segment_index >= segments_count )
						{
							pos  = float2(0,0);
							#if TWO_TRIS_PER_SEGMENT
							rel_pos = pos;
							ab_len = 0;
							#else
							dist = 0.5f;
							#endif
							return false;
						}
						uint v_index_seg_rel = index - segment_index * VERTS_PER_SEGMENT;

						float2 A = read_segment( segment_index, 0 );
						float2 B = read_segment( segment_index, 1 );

						float2 vec   = B - A;
						float  _ab_len = length( vec );
						float2 fw    = _ab_len == 0 ? vec : vec / _ab_len;
						float2 right = float2(fw.y, -fw.x);
						float2 rr = right * radius;
						float2 fr = fw * radius;
						#define SQRT2 1.41421356237309504880
						float dist_far = SQRT2 * radius;


						#if TWO_TRIS_PER_SEGMENT
						float2 verts[VERTS_PER_SEGMENT] = {
						/* E */ A - fr - rr,
						/* H */ B + fr - rr,
						/* I */ A - fr + rr,
						/* L */ B + fr + rr,
						};
						#else
						float2 verts[VERTS_PER_SEGMENT] = {
						/* A */ A          ,
						/* B */ B          ,
						/* C */ A - fr     ,
						/* D */ B + fr     ,
						/* E */ A - fr - rr,
						/* F */ A - rr     ,
						/* G */ B - rr     ,
						/* H */ B + fr - rr,
						/* I */ A - fr + rr,
						/* J */ A + rr     ,
						/* K */ B + rr     ,
						/* L */ B + fr + rr,
						};

						float dists[VERTS_PER_SEGMENT] = {
						/* A */ 0       ,
						/* B */ 0       ,
						/* C */ radius  ,
						/* D */ radius  ,
						/* E */ dist_far,
						/* F */ radius  ,
						/* G */ radius  ,
						/* H */ dist_far,
						/* I */ dist_far,
						/* J */ radius  ,
						/* K */ radius  ,
						/* L */ dist_far,
						};
						#endif

						float2 _pos = float2(0,0);
						#if TWO_TRIS_PER_SEGMENT
						#else
						float _dist = 0;
						#endif

						for ( uint i = 0; i != VERTS_PER_SEGMENT; ++i )
						{
							if ( i == v_index_seg_rel )
							{
								#if TWO_TRIS_PER_SEGMENT
								_pos  = verts[i];
								#else
								_pos  = verts[i];
								_dist = dists[i];
								#endif
							}
						}

						#if TWO_TRIS_PER_SEGMENT
						#else
						_dist /= radius;
						#endif

						//#if ENCODED_POS
						//_pos = mul( pos_encode_matrix, float4( _pos, 0, 1 ) ).xy;
						//#endif

						pos  = _pos;

						#if TWO_TRIS_PER_SEGMENT
						float2 _rel_p = _pos - A;
						float x = dot( _rel_p, fw );
						float y = dot( _rel_p, -right );
						rel_pos = float2( x, y );
						ab_len  = _ab_len;
						#else
						dist = _dist;
						#endif

						return true;
					}
				#endif

				#if TWO_TRIS_PER_SEGMENT
				bool read_pos_dist( uint index, out float2 pos, out float2 rel_pos, out float ab_len )
				#else
				bool read_pos_dist( uint index, out float2 pos, out float dist )
				#endif
				{
					#if COMPUTE_POS_DIST

					#if TWO_TRIS_PER_SEGMENT
					return compute_pos_dist( index, pos, rel_pos, ab_len );
					#else
					return compute_pos_dist( index, pos, dist );
					#endif

					#else

					#if TWO_TRIS_PER_SEGMENT
					pos  = float2(0,0);
					rel_pos = pos;
					ab_len = 0;
					return false;
					#else

					uint i3 = index * 3;
					float2 bv;
					float d;

					#if WPOSDIST_F4

					float4 f4 = _PosDistBuf.Load( index );
					bv.x = f4.x;
					bv.y = f4.y;
					d = f4.z;

					#elif WPOSDIST_TEXTURE

					uint2 i2;
					i2.y = index / 1024u;
					i2.x = index - i2.y * 1024u;
					float4 f4 = _PosDistBuf[i2];
					bv.x = f4.x;
					bv.y = f4.y;
					d = f4.z;

					#else

					bv.x = _PosDistBuf.Load( i3 + 0 );
					bv.y = _PosDistBuf.Load( i3 + 1 );
					d = _PosDistBuf.Load( i3 + 2 );

					#endif

					#if ENCODED_POS

					pos = mul( pos_decode_matrix, float4( bv, 0, 1 ) ).xy;

					#else

					pos = bv;

					#endif

					dist = d * radius;

					#endif

					#endif

					return true;
				}

			#endif

			#if VERTS_GEN_CPU
			#if TWO_TRIS_PER_SEGMENT
			v2f vert( float4 in_ws_pos : POSITION, float2 rel_pos : TEXCOORD0, float2 in_ab_len : TEXCOORD1 )
			{
				v2f o;
				float ab_len = in_ab_len.x;
				float3 ws_pos = in_ws_pos.xyz;
			#else
			v2f vert( float4 in_ws_pos : POSITION, float2 in_dist : TEXCOORD0, uint vertex_index : SV_VertexID )
			{
				v2f o;
				float dist = in_dist.x;
				float3 ws_pos = in_ws_pos.xyz;

				uint   v_index = vertex_index;

				uint   seg_rel_v_index = v_index % VERTICES_PER_SEGMENT;
				uint   seg_v_index_start = v_index - seg_rel_v_index;
				uint   closest_index_to[12] = {0,1,0,1,0,0,1,1,0,0,1,1};
				uint   closest_index = seg_v_index_start + closest_index_to[seg_rel_v_index];
			#endif
			#else
			v2f vert( uint vertex_index : SV_VertexID )
			{
				v2f o;

				#if INDIRECT_RENDERING
				uint   v_index = _IndexBuf.Load( vertex_index );
				#else
				uint   v_index = vertex_index;
				#endif

				#if TWO_TRIS_PER_SEGMENT
				float2 pos;
				float2 rel_pos;
				float  ab_len;
				read_pos_dist( v_index, pos, rel_pos, ab_len );
				#else
				uint   seg_rel_v_index = v_index % VERTICES_PER_SEGMENT;
				uint   seg_v_index_start = v_index - seg_rel_v_index;
				uint   closest_index_to[12] = {0,1,0,1,0,0,1,1,0,0,1,1};
				uint   closest_index = seg_v_index_start + closest_index_to[seg_rel_v_index];

				float2 pos;
				float dist;
				read_pos_dist( v_index, pos, dist );
				#endif

				float3 ws_pos = float3( pos, 0 );
			#endif

				float4 cs_pos = UnityObjectToClipPos( ws_pos );
				//cs_pos.xy = pos.xy * 2 / float2(1600,256) - 1;
				//cs_pos.z = 0;
				//cs_pos.w = 1;
				//cs_pos.y *= 32;

				o.vertex        = cs_pos;

				#if TWO_TRIS_PER_SEGMENT
				o.ss_vertex     = rel_pos;
				o.ab_len        = ab_len;
				#else
				o.dist          = dist;
				o.closest_index = float(closest_index);
				o.ws_vertex     = ws_pos.xy;
				#endif


				#if ENABLE_DEBUG_BUF
				//dbg.z = float(vertex_index);
				//dbg.w = float(v_index);
				float4 dbg = float4(cs_pos);
				_DebugVertexBuf[vertex_index] = dbg;
				//#if 0
				//_DebugVertexBuf[0] = UNITY_MATRIX_MVP[0];
				//_DebugVertexBuf[1] = UNITY_MATRIX_MVP[1];
				//_DebugVertexBuf[2] = UNITY_MATRIX_MVP[2];
				//_DebugVertexBuf[3] = UNITY_MATRIX_MVP[3];
				//#endif
				#endif

				return o;
			}

			float sdSegment( in float2 p, in float2 a, in float2 b )
			{
				float2 pa = p - a, ba = b - a;
				float h = clamp( dot( pa,ba ) / dot( ba,ba ), 0.0, 1.0 );
				return length( pa - ba * h );
			}
			float sdSegmentAxisAligned( in float2 p, in float ab_len )
			{
				//float2 b = float2(ab_len,0);
				//float2 pa = p, ba = b;
				//float h = clamp( dot( pa,ba ) / dot( ba,ba ), 0.0, 1.0 );
				//float h = clamp( pa.x * b.x / ba.x * ba.x, 0.0, 1.0 );
				//return length( float2( pa.x - ba.x * h, pa.y ) );
				float h = clamp( p.x / ab_len, 0.0, 1.0 );
				return length( float2( p.x - ab_len * h, p.y ) );
			}
			float4 frag( v2f i, out float depth : SV_Depth ) : SV_Target
			{
				#if TWO_TRIS_PER_SEGMENT
				float distance = sdSegmentAxisAligned( i.ss_vertex, i.ab_len );
				#else
				float distance = i.dist;
				if ( frac( i.closest_index ) == 0 )
				{
					float2 closest_pos;
					float dist;
					read_pos_dist( i.closest_index, closest_pos, dist );
					distance = length( i.ws_vertex - closest_pos );
				}
				#endif

				float depth01 = min( 1, distance / radius );
				float4 output = float4( depth01, depth01, depth01, 1 );

				//#if defined(UNITY_REVERSED_Z)
				//depth = 1 - depth01;
				//#else
				depth = depth01;
				//#endif

				return output;
			}
			ENDCG
		}
	}
}
