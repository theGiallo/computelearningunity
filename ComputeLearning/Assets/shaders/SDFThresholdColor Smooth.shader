Shader "tg/sdf/ThresholdColorSmooth"
{
	Properties
	{
		_MainTex         ( "Texture",             2D ) = "white" {}
		_ColorIn         ( "Color In",            Color ) = ( 0,0,0,1 )
		_ColorOut        ( "Color Out",           Color ) = ( 1,1,1,1 )
		_RadiusThreshold ( "Distance  Threshold", Float ) = -1
		_SmoothRange     ( "Smooth Range",        Float ) = 1.0
	}
		SubShader
		{
			// No culling or depth
			Cull Off ZWrite Off ZTest Always ZClip False

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert ( appdata v )
				{
					v2f o;
					o.vertex = UnityObjectToClipPos( v.vertex );
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				float _RadiusThreshold;
				fixed4 _ColorIn;
				fixed4 _ColorOut;
				float _SmoothRange;

				float radius;

				fixed4 frag ( v2f i ) : SV_Target
				{
					fixed4 col;// = tex2D(_MainTex, i.uv);

					float depth =
					//#if defined(UNITY_REVERSED_Z)
					//1 -
					//#endif
					SAMPLE_DEPTH_TEXTURE( _MainTex, i.uv );

					//col = depth * radius < _RadiusThreshold ? _ColorIn : _ColorOut;
					float th = _RadiusThreshold < 0 ? radius - _SmoothRange : _RadiusThreshold;
					float sdf = depth * radius - th;
					//float sr = _SmoothRange * fwidth( depth );
					float sr = _SmoothRange * length( float2( ddx(sdf), ddy(sdf) ) );
					col = lerp( _ColorIn, _ColorOut, smoothstep( -sr, sr, sdf ) );
					return col;
				}
				ENDCG
			}
		}
}
