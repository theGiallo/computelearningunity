Shader "Hidden/SegsToTris"
{
	Properties
	{
		//_MainTex ( "Texture", 2D ) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_local POLYLINE SEGMENTS
			#pragma multi_compile_local SEGMENTS_IN_TEXTURE __

			#include "UnityCG.cginc"

			#if 1//SHADER_API_MOBILE
			#define ENCODED_POS 1
			#endif

			uniform float  radius;
			uniform uint   segments_count;
			uniform float4 image_size;
			#if SEGMENTS_IN_TEXTURE
			uniform Texture2D<float2> read_segments_tex : register(t0); // (start(xy), end(xy))
			uniform uint read_segments_tex_width;
			#else
			uniform Buffer<float2> read_segments  : register(t0); // (start(xy), end(xy))
			#endif


			#if ENCODED_POS
			uniform float4x4 pos_encode_matrix;
			#endif

			float2 read_segment( uint segment_index, uint ab01 )
			{
				#if POLYLINE
				uint i = segment_index + ab01;
				#else
				uint i = segment_index * 2 + ab01;
				#endif

				#if SEGMENTS_IN_TEXTURE
				uint2 i2;
				uint w = (uint)read_segments_tex_width;
				i2.y = i / w;
				i2.x = i - i2.y * w;
				//i2.y = 1023 - i2.y;
				float2 ret = read_segments_tex.Load( uint3(i2,0) );
				#else
				float2 ret = read_segments[i];
				#endif

				return ret;
			}



			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert ( appdata v )
			{
				v2f o;
				o.vertex = UnityObjectToClipPos( v.vertex );
				o.uv = v.uv;
				return o;
			}

			//sampler2D _MainTex;

			float4 frag ( v2f i ) : SV_Target
			{
				//#if UNITY_UV_STARTS_AT_TOP
				//if ( _MainTex_TexelSize.y < 0 )
				//	uv.y = 1 - uv.y;
				//#endif

				uint2 tex_size = uint2(image_size.xy);
				uint2 pixel_xy = uint2( floor( i.uv * tex_size ) );

				uint v_index = pixel_xy.x + pixel_xy.y * tex_size.x;
				#define VERTS_PER_SEGMENT 12
				uint segment_index = v_index / VERTS_PER_SEGMENT;
				if ( segment_index >= segments_count )
				{
					discard;
				}
				uint v_index_seg_rel = v_index - segment_index * VERTS_PER_SEGMENT;

				float2 A = read_segment( segment_index, 0 );
				float2 B = read_segment( segment_index, 1 );

				float2 vec   = B - A;
				float2 fw    = normalize( vec );
				float2 right = float2(fw.y, -fw.x);
				float2 rr = right * radius;
				float2 fr = fw * radius;
				#define SQRT2 1.41421356237309504880
				float dist_far = SQRT2 * radius;


				float2 verts[VERTS_PER_SEGMENT] = {
				/* A */ A          ,
				/* B */ B          ,
				/* C */ A - fr     ,
				/* D */ B + fr     ,
				/* E */ A - fr - rr,
				/* F */ A - rr     ,
				/* G */ B - rr     ,
				/* H */ B + fr - rr,
				/* I */ A - fr + rr,
				/* J */ A + rr     ,
				/* K */ B + rr     ,
				/* L */ B + fr + rr,
				};

				float dists[VERTS_PER_SEGMENT] = {
				/* A */ 0       ,
				/* B */ 0       ,
				/* C */ radius  ,
				/* D */ radius  ,
				/* E */ dist_far,
				/* F */ radius  ,
				/* G */ radius  ,
				/* H */ dist_far,
				/* I */ dist_far,
				/* J */ radius  ,
				/* K */ radius  ,
				/* L */ dist_far,
				};

				float dist = 0;
				float2 pos = float2(0,0);

				for ( uint i = 0; i != VERTS_PER_SEGMENT; ++i )
				{
					if ( i == v_index_seg_rel )
					{
						pos  = verts[i];
						dist = dists[i];
					}
				}

				dist /= radius;

				#if ENCODED_POS
				pos = mul( pos_encode_matrix, float4( pos, 0, 1 ) ).xy;
				#endif

				float4 f4 = float4( pos.x, pos.y, dist, 0 );

				return f4;
			}
			ENDCG
		}
	}
}
