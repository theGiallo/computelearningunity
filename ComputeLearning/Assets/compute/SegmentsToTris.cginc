#if SHADER_API_MOBILE
#define UAV_FTYPE  RWBuffer   <float >
#define UAV_FTYPE2 RWTexture2D<float4>
#define ENCODE_UAV_INDEX2(i) uint2((i)-(i)/uint(1024),(i)/uint(1024))
#define ENCODE_UAV_INDEX(i) i//uint2((i)-(i)/uint(1024),(i)/uint(1024))
#define ENCODE_F2(f2) mul( pos_encode_matrix, float4( f2, 0, 1 ) )
#define SHADER_FTYPE  StructuredBuffer<float >
#define SHADER_FTYPE2 StructuredBuffer<float4>
#define SHADER_DECODE_F2_TO_F3(buf_value) mul( pos_decode_matrix, float4( buf_value.xy, 0, 1 ) ).xyz
#else
#define UAV_FTYPE  RWBuffer<float >
#define UAV_FTYPE2 RWBuffer<float2>
#define ENCODE_UAV_INDEX2(i) i
#define ENCODE_UAV_INDEX(i) i
#define ENCODE_F2(f2) f2
#define SHADER_FTYPE  StructuredBuffer<float >
#define SHADER_FTYPE2 StructuredBuffer<float2>
#define SHADER_DECODE_F2_TO_F3(buf_value) float3( buf_value.xy, 0 )
#endif
