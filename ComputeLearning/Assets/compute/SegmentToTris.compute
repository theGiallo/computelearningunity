#if 1//SHADER_API_MOBILE
#define ENCODED_POS 1
#endif

#if 1//SHADER_API_MOBILE
#define WPOSDIST_TEXTURE 1
#endif

// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain
//#pragma kernel CSMain POLYLINE
#pragma multi_compile_local POLYLINE SEGMENTS
#pragma multi_compile_local SEGMENTS_IN_TEXTURE __

//#include "SegmentsToTris.cginc"

#if SEGMENTS_IN_TEXTURE
uniform Texture2D<float2> read_segments_tex  : register(t0); // (start(xy), end(xy))
uniform uint read_segments_tex_width;
#else
uniform Buffer<float2> read_segments  : register(t0); // (start(xy), end(xy))
#endif


#if WPOSDIST_F4
uniform RWBuffer<float4> write_pos_dist : register(u1); // vertex pos.xy, distance
#elif WPOSDIST_TEXTURE
uniform RWTexture2D<float4> write_pos_dist : register(u1); // vertex pos.xy, distance
#else
uniform RWBuffer<float > write_pos_dist : register(u1); // vertex pos.xy, distance
#endif

#if SHADER_GEN_INDEXES
uniform RWBuffer<int   > write_indexes  : register(u2); // triangle indices
#endif

#if ENCODED_POS
uniform float4x4 pos_encode_matrix;
#endif
uniform float radius;
uniform int   segments_count;

/*
	E----F------------G----H
	|.   |         ...|   .|
	| .  |      ...   |  . |
	|  . |   ...      | .  |
	|   .|...         |.   |
	C----A------------B----D
	|   .|         ...|.   |
	|  . |      ...   | .  |
	| .  |   ...      |  . |
	|.   |...         |   .|
	I----J------------K----L

	Segment is A-B, radius is |CA| == |AJ| == |FA| == |BD|


	E----------------------H
	|                    ..|
	|                 ...  |
	|               ..     |
	|            ...       |
	-----A---- .. ----B-----
	|       ...            |
	|     ..               |
	|  ...                 |
	|..                    |
	I----------------------L

	Two triangles version.

*/

float2 read_segment( uint segment_index, uint ab01 )
{
	#if POLYLINE
	uint i = segment_index + ab01;
	#else
	uint i = segment_index * 2 + ab01;
	#endif

	#if SEGMENTS_IN_TEXTURE
	uint2 i2;
	uint w = (uint)read_segments_tex_width;
	i2.y = i / w;
	i2.x = i - i2.y * w;
	//i2.y = 1023 - i2.y;
	float2 ret = read_segments_tex.Load( uint3(i2,0) );
	#else
	float2 ret = read_segments[i];
	#endif

	return ret;
}



void do_write_pos_dist( int index, float2 pos, float dist )
{
	dist /= radius;

	uint i = index * 3;

	#if ENCODED_POS
	pos = mul( pos_encode_matrix, float4( pos, 0, 1 ) ).xy;
	#endif


	#if WPOSDIST_F4

	float4 f4 = float4( pos.x, pos.y, dist, 0 );
	write_pos_dist[index] = f4;

	#elif WPOSDIST_TEXTURE

	uint2 i2;
	i2.y = index / 1024u;
	i2.x = index - i2.y * 1024u;
	float4 f4 = float4( pos.x, pos.y, dist, 0 );
	write_pos_dist[i2] = f4;

	#else

	write_pos_dist[i + 0] = pos.x;
	write_pos_dist[i + 1] = pos.y;
	write_pos_dist[i + 2] = dist ;

	#endif
}


[numthreads( 32, 1, 1 )]
void CSMain ( uint3 id : SV_DispatchThreadID )
{
	if ( (int)id.x >= segments_count ) return;

	// Get the dimensions of the RenderTexture
	//uint width, height;
	//read_texture.GetDimensions( width, height );
	//uint2 size = uint2( uint(width), uint(height) );

	float2 A = read_segment( id.x, 0 );
	float2 B = read_segment( id.x, 1 );

	float2 vec   = B - A;
	float2 fw    = normalize( vec );
	float2 right = float2(fw.y, -fw.x);
	float2 rr = right * radius;
	float2 fr = fw * radius;
	#define SQRT2 1.41421356237309504880
	float dist_far = SQRT2 * radius;

	#define VERTS_PER_SEGMENT 12
	#define TRIANGLES_PER_SEGMENT 12
	#define INDEXES_PER_SEGMENT TRIANGLES_PER_SEGMENT * 3
	int idx_start = id.x * VERTS_PER_SEGMENT; //id.y * width + id.x;
	int triangles_idx_start = id.x * INDEXES_PER_SEGMENT;

	int A_idx =  0 + idx_start;
	int B_idx =  1 + idx_start;
	int C_idx =  2 + idx_start;
	int D_idx =  3 + idx_start;
	int E_idx =  4 + idx_start;
	int F_idx =  5 + idx_start;
	int G_idx =  6 + idx_start;
	int H_idx =  7 + idx_start;
	int I_idx =  8 + idx_start;
	int J_idx =  9 + idx_start;
	int K_idx = 10 + idx_start;
	int L_idx = 11 + idx_start;

	float2 A2 = A          ;
	float2 B2 = B          ;
	float2 E2 = A - fr - rr;
	float2 I2 = A - fr + rr;
	float2 H2 = B + fr - rr;
	float2 L2 = B + fr + rr;
	float2 C2 = A - fr     ;
	float2 D2 = B + fr     ;
	float2 F2 = A - rr     ;
	float2 J2 = A + rr     ;
	float2 G2 = B - rr     ;
	float2 K2 = B + rr     ;

	float3 A3 = float3(A2.x, A2.y, 0);
	float3 B3 = float3(B2.x, B2.y, 0);
	float3 C3 = float3(C2.x, C2.y, 0);
	float3 D3 = float3(D2.x, D2.y, 0);
	float3 E3 = float3(E2.x, E2.y, 0);
	float3 F3 = float3(F2.x, F2.y, 0);
	float3 G3 = float3(G2.x, G2.y, 0);
	float3 H3 = float3(H2.x, H2.y, 0);
	float3 I3 = float3(I2.x, I2.y, 0);
	float3 J3 = float3(J2.x, J2.y, 0);
	float3 K3 = float3(K2.x, K2.y, 0);
	float3 L3 = float3(L2.x, L2.y, 0);


	do_write_pos_dist( A_idx, A2, 0        );
	do_write_pos_dist( B_idx, B2, 0        );
	do_write_pos_dist( E_idx, E2, dist_far );
	do_write_pos_dist( I_idx, I2, dist_far );
	do_write_pos_dist( H_idx, H2, dist_far );
	do_write_pos_dist( L_idx, L2, dist_far );
	do_write_pos_dist( C_idx, C2, radius   );
	do_write_pos_dist( D_idx, D2, radius   );
	do_write_pos_dist( F_idx, F2, radius   );
	do_write_pos_dist( G_idx, G2, radius   );
	do_write_pos_dist( J_idx, J2, radius   );
	do_write_pos_dist( K_idx, K2, radius   );

	#if SHADER_GEN_INDEXES
	int ti = 0;
	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = E_idx;
	write_indexes[triangles_idx_start + ti++] = C_idx;

	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = F_idx;
	write_indexes[triangles_idx_start + ti++] = E_idx;

	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = G_idx;
	write_indexes[triangles_idx_start + ti++] = F_idx;

	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = B_idx;
	write_indexes[triangles_idx_start + ti++] = G_idx;

	write_indexes[triangles_idx_start + ti++] = B_idx;
	write_indexes[triangles_idx_start + ti++] = H_idx;
	write_indexes[triangles_idx_start + ti++] = G_idx;

	write_indexes[triangles_idx_start + ti++] = B_idx;
	write_indexes[triangles_idx_start + ti++] = D_idx;
	write_indexes[triangles_idx_start + ti++] = H_idx;

	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = C_idx;
	write_indexes[triangles_idx_start + ti++] = I_idx;

	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = I_idx;
	write_indexes[triangles_idx_start + ti++] = J_idx;

	write_indexes[triangles_idx_start + ti++] = A_idx;
	write_indexes[triangles_idx_start + ti++] = J_idx;
	write_indexes[triangles_idx_start + ti++] = B_idx;

	write_indexes[triangles_idx_start + ti++] = B_idx;
	write_indexes[triangles_idx_start + ti++] = J_idx;
	write_indexes[triangles_idx_start + ti++] = K_idx;

	write_indexes[triangles_idx_start + ti++] = B_idx;
	write_indexes[triangles_idx_start + ti++] = K_idx;
	write_indexes[triangles_idx_start + ti++] = L_idx;

	write_indexes[triangles_idx_start + ti++] = B_idx;
	write_indexes[triangles_idx_start + ti++] = L_idx;
	write_indexes[triangles_idx_start + ti++] = D_idx;
	#endif

	// Transform pixel to [-1,1] range
	//float2 uv = float2( ( id.xy + float2( 0.5f, 0.5f ) ) / float2( width, height ) * 2.0f - 1.0f );

	//float3 v3 = read_texture[id.xy];
	//write_texture[id.xy] = output;
}
