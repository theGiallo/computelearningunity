namespace tg
{
	public class Squirrel3
	{
		public int  position;
		public uint seed;

		public Squirrel3()
		{
			position = 0;
			seed = randomSeed();
		}
		public static uint randomSeed() => unchecked ( (uint) ( new System.Random().Next() ) );
		public Squirrel3( uint seed )
		{
			position = 0;
			this.seed = seed;
		}

		public uint LastUInt()
		{
			uint ret = squirrel3( position, seed );
			return ret;
		}
		public uint NextUInt()
		{
			uint ret = squirrel3( position, seed );
			++position;
			return ret;
		}
		public int NextInt()
		{
			int ret = unchecked ( (int)NextUInt() );
			return ret;
		}
		public int LastInt()
		{
			int ret = unchecked ( (int)LastUInt() );
			return ret;
		}
		public float NextFloat01()
		{
			float ret;
			uint u = NextUInt();
			ret = (float) ( u / (double) uint.MaxValue );
			return ret;
		}
		public float NextFloat01NAVG( int n = 4 )
		{
			float ret = 0;
			for ( int i = 0; i != n; ++i )
			{
				ret = ret + NextFloat01();
			}
			ret /= (float)n;
			return ret;
		}
		public float NextFloatM1P1()
		{
			float ret;
			int i = NextInt();
			ret = (float) ( i / (double) ( i > 0 ? int.MaxValue : int.MinValue ) );
			return ret;
		}
		public float NextRangeIncl( float min, float max )
		{
			float ret;
			float m1p1 = NextFloatM1P1();
			//float half_size = ( max - min ) * 0.5f;
			//float center = ( min + max ) * 0.5f;
			//ret = center + m1p1 * half_size;
			//ret = 0.5f * ( min * ( 1 - m1p1 ) + max * ( 1 + ret ) );
			ret = 0.5f * ( ( min + max ) + m1p1 * ( max - min ) );
			return ret;
		}
		public int NextRangeIncl( int min, int max )
		{
			int ret;
			double dmin = min - 0.5;
			double dmax = max + 0.5;
			int i = NextInt();
			double dm1p1 = ( i / ( i > 0 ? (double)int.MaxValue : -(double)int.MinValue ) );
			double d = 0.5 * ( ( dmin + dmax ) + dm1p1 * ( dmax - dmin ) );
			ret = (int)System.Math.Round( d );
			return ret;
		}

		uint squirrel3( int position, uint seed )
		{

			const uint BIT_NOISE1 = 0x68E31DA4;
			const uint BIT_NOISE2 = 0xB5297A4D;
			const uint BIT_NOISE3 = 0x1B56C4E9;

			uint mangled = unchecked ( (uint)position );
			mangled *= BIT_NOISE1;
			mangled += seed;
			mangled ^= mangled >> 8;
			mangled += BIT_NOISE2;
			mangled ^= mangled << 8;
			mangled *= BIT_NOISE3;
			mangled ^= mangled >> 8;
			return mangled;
		}
	}
}
