using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using tg;
using ButtonAttribute = NaughtyAttributes.ButtonAttribute;

public class SimplePlot : MonoBehaviour
{
	[Serializable]
	public struct GridValue
	{
		public string name;
		public float  value;

		public string GetLabel()
		{
			return string.IsNullOrWhiteSpace( name ) ? $"{value:000.000}" : name;
		}
	}

	public DrawSegments  segmentsDrawer;
	public RawImage      valuePlotRawImage;
	public RawImage      gridPlotRawImage;
	public string        xAxisUnits;
	public string        yAxisUnits;
	public TMP_Text      xAxisMinText;
	public TMP_Text      xAxisMaxText;
	public TMP_Text      yAxisMinText;
	public TMP_Text      yAxisMaxText;
	public TMP_Text      maxValueText;
	public TMP_Text      lastValueText;
	public TMP_Text      labelPrototype;
	public RectTransform plot_rt;

	[Serializable]
	public struct DynamicValue
	{
		public bool  isDynamic;
		[HideIf("isDynamic"),AllowNesting]
		public float value;

		public float eval( float dynamic_value ) => isDynamic ? dynamic_value : value;
	}
	[Serializable]
	public struct DynamicRange
	{
		public DynamicValue min;
		public DynamicValue max;
	}
	public DynamicRange plotXRange;
	public DynamicRange plotYRange;

	public bool  samplesMaxCountByDensity;
	[HideIf("samplesMaxCountByDensity")]
	public int   fixedSamplesMaxCount;
	[ShowIf("samplesMaxCountByDensity")]
	public float minXSamplesSpacing;
	public bool xIsIndex;
	public bool xAxisZeroRight;
	public bool startFilled;
	public bool fillIndex;
	[ShowIf("startFilled")]
	public Vector2 startFillValue;
	public bool forceSamplesCountInRange;

	public int maxSamples() => samplesMaxCountByDensity
	                         ? Mathf.FloorToInt( width / minXSamplesSpacing )
	                         : fixedSamplesMaxCount;

	public GridValue[] gridYValues;

	public Material plotMaterial;
	public Material gridMaterial;
	public int samplesCount { get; private set; }

	public Vector2 valuesRangeMin => values_range_min;
	public Vector2 valuesRangeMax => values_range_max;

	public void Clear()
	{
		samplesCount = 0;
		values_range_min = new Vector2();
		values_range_max = new Vector2();

		var pl = segmentsDrawer.polyline;

		if ( startFilled )
		{
			check_initial_fill();
		}
		else
		{
			pl.Clear();
		}
	}
	public void AddSample( float value_y )
	{
		AddSample( new Vector2( samplesCount, value_y ) );
	}
	public void AddSample( Vector2 value, bool refresh_plot = true )
	{
		if ( values.Length == 0 )
		{
			return;
		}

		values_range_min = new Vector2( float.MaxValue, float.MaxValue );
		values_range_max = new Vector2( float.MinValue, float.MinValue );

		++samplesCount;

		if ( samplesCount > values.Length )
		{
			if ( forceSamplesCountInRange )
			{
				samplesCount = values.Length;
			}

			for ( int i = 0; i != values.Length - 1; ++i )
			{
				var f = values[i + 1];
				if ( xIsIndex ) f.x = i;
				values[i] = f;

				update_ranges( f );
			}

			if ( xIsIndex ) value.x = values.Length - 1;
			values[values.Length - 1] = value;

			update_ranges( value );
		}
		else
		{
			int index = samplesCount - 1;
			if ( xIsIndex ) value.x = index;
			values[index] = value;
			update_ranges( values[index] );
		}

		if ( refresh_plot )
		{
			refreshPlotValues();
		}
	}
	#if UNITY_EDITOR
	private bool is_editor_mode() =>!Application.isPlaying;
	#endif
	[Button,HideIf("is_editor_mode")]
	public void refreshPlotValues()
	{
		var pl = segmentsDrawer.polyline;

		int count = startFilled ? values.Length : Mathf.Min( values.Length, samplesCount );

		if ( pl.Count < count )
		{
			if ( pl.Capacity < count )
			{
				pl.Capacity = count;
			}
			while ( pl.Count < count )
			{
				pl.Add( Vector2.zero );
			}
		}

		for ( int i = 0; i != count; ++i )
		{
			pl[i] = valueToPixelCoords( values[i] );
		}
	}

	#region Unity
	private void Awake()
	{
		segmentsDrawer.width  = Mathf.CeilToInt( plot_rt.rect.width  );
		segmentsDrawer.height = Mathf.CeilToInt( plot_rt.rect.height );
	}
	private void Start()
	{
		segmentsDrawer.mode = DrawSegments.Mode.Polyline;
		height  = segmentsDrawer.height;
		width   = segmentsDrawer.width;
		width_f = segmentsDrawer.width;
		segmentsDrawer.autoUpdate = false;

		valuePlotRawImage_rectTransform = valuePlotRawImage.rectTransform;

		values                      = new Vector2[maxSamples()];
		grid_labels                 = new TMP_Text      [gridYValues.Length];
		grid_labels_transforms      = new Transform     [gridYValues.Length];
		grid_labels_rect_transforms = new RectTransform [gridYValues.Length];
		grid_segments               = new List<DrawSegments.Segment>();

		{
			var pl = segmentsDrawer.polyline;
			pl.Clear();

			check_initial_fill();
		}

		var labelPrototype_go = labelPrototype.gameObject;
		for ( int i = 0; i != gridYValues.Length; ++i )
		{
			var label = Instantiate( labelPrototype_go, labelPrototype.transform.parent );
			grid_labels[i] = label.GetComponent<TMP_Text>();
			var gv = gridYValues[i];
			grid_labels[i].text = gv.GetLabel();
			grid_labels_transforms[i] = label.transform;
			grid_labels_rect_transforms[i] = label.GetComponent<RectTransform>();
			grid_segments.Add( new DrawSegments.Segment() );
		}
		labelPrototype_go.SetActive( false );

		value_plot_texture = new Texture2D( segmentsDrawer.width, segmentsDrawer.height, TextureFormat.ARGB32, false );
		grid_plot_texture  = new Texture2D( segmentsDrawer.width, segmentsDrawer.height, TextureFormat.ARGB32, false );
	}
	private void LateUpdate()
	{
		if ( _profile )
		{
			MonotonicTimestamp start = MonotonicTimestamp.Now();
			draw();
			MonotonicTimestamp end = MonotonicTimestamp.Now();
			_draw_time = (float)( end - start ).TotalSeconds;
		}
		else
		{
			draw();
		}
	}
	#endregion Unity

	#region private
	private int height;
	private int width;
	private float width_f;
	[NonSerialized,ShowNonSerializedField]
	public float _draw_time;
	[NonSerialized]
	public bool   _profile = false;
	#if UNITY_EDITOR
	[Button]
	private void toggleProfile()
	{
		_profile = ! _profile;
	}
	#endif

	Vector2 values_range_min = new Vector2(float.MaxValue,float.MaxValue);
	Vector2 values_range_max = new Vector2(float.MinValue,float.MinValue);

	private TMP_Text[]                 grid_labels;
	private Transform[]                grid_labels_transforms;
	private RectTransform[]            grid_labels_rect_transforms;
	private List<DrawSegments.Segment> grid_segments;
	private Vector2[]                  values = null;

	private Texture2D value_plot_texture;
	private Texture2D grid_plot_texture;

	private RectTransform valuePlotRawImage_rectTransform;
	private Vector2 valueToPixelCoords( Vector2 value )
	{
		Vector2 min = new Vector2( plotXRange.min.eval( values_range_min.x ),
		                           plotYRange.min.eval( values_range_min.y ) );
		Vector2 max = new Vector2( plotXRange.max.eval( values_range_max.x ),
		                           plotYRange.max.eval( values_range_max.y ) );

		Vector2 range = max - min;
		Vector2 border = new Vector2( 1, 1 );
		Vector2 offset = border;
		Vector2 pixels_range = new Vector2( width_f, height ) - ( 2 * border );

		range.x = float.IsNaN( range.x ) ? 0 : range.x;
		range.y = float.IsNaN( range.y ) ? 0 : range.y;

		Vector2 clamped_range;
		clamped_range.x = range.x <= 0 ? 0 : range.x;
		clamped_range.y = range.y <= 0 ? 0 : range.y;

		Vector2 fixed_range;
		fixed_range.x = range.x <= 0 ? 1 : range.x;
		fixed_range.y = range.y <= 0 ? 1 : range.y;

		Vector2 ret = ( ( value - min ) / fixed_range ) * pixels_range;

		if ( clamped_range.x == 0 )
		{
			ret.x = value.x == min.x ? pixels_range.x * 0.5f : - pixels_range.x - offset.x;
		}

		if ( clamped_range.y == 0 )
		{
			ret.y = value.y == min.y ? pixels_range.y * 0.5f : - pixels_range.y - offset.y;
		}

		ret += offset;

		return ret;
	}

	private void draw()
	{
		if ( values.Length == 0 )
		{
			return;
		}

		bool has_values = samplesCount != 0;
		if ( maxValueText  != null ) maxValueText .text = has_values ? $"{values_range_max.y:000.000} {yAxisUnits}" : "";
		if ( lastValueText != null ) lastValueText.text = has_values ? $"{values[Mathf.Min(values.Length, samplesCount ) - 1].y:000.000} {yAxisUnits}" : "";

		float xmin = plotXRange.min.eval( values_range_min.x );
		float xmax = plotXRange.max.eval( values_range_max.x );
		float ymin = plotYRange.min.eval( values_range_min.y );
		float ymax = plotYRange.max.eval( values_range_max.y );
		if ( xAxisZeroRight )
		{
			xmin -= xmax;
			xmax = 0;
		}
		if ( xAxisMinText != null ) xAxisMinText.text = has_values ? $"{xmin} {xAxisUnits}" : "";
		if ( xAxisMaxText != null ) xAxisMaxText.text = has_values ? $"{xmax} {xAxisUnits}" : "";
		if ( yAxisMinText != null ) yAxisMinText.text = has_values ? $"{ymin} {yAxisUnits}" : "";
		if ( yAxisMaxText != null ) yAxisMaxText.text = has_values ? $"{ymax} {yAxisUnits}" : "";

		segmentsDrawer.mode = DrawSegments.Mode.Polyline;
		segmentsDrawer.sdfShadingMaterial = plotMaterial;
		segmentsDrawer.draw();
		Graphics.CopyTexture( src: segmentsDrawer.renderedTexture, dst: value_plot_texture );

		var old_segments = segmentsDrawer.segments;

		float img_h = valuePlotRawImage_rectTransform.rect.height;
		for ( int i = 0; i != gridYValues.Length; ++i )
		{
			var start = valueToPixelCoords( new Vector2( 0, gridYValues[i].value ) );
			var end = start;
			end.x = width - 1;//( max_total_samples_count - 1 ) * minPixelsPerSample;
			grid_segments[i] = new DrawSegments.Segment( start, end );

			var ap = grid_labels_rect_transforms[i].anchoredPosition;

			float y_pc = start.y / (float)( height - 1 );
			float y = y_pc * img_h;
			ap.y = y;

			grid_labels_rect_transforms[i].anchoredPosition = ap;

			grid_labels[i].enabled = y_pc >= 0 && y_pc <= 1;
		}

		segmentsDrawer.segments = grid_segments;
		segmentsDrawer.mode = DrawSegments.Mode.Segments;

		segmentsDrawer.sdfShadingMaterial = gridMaterial;
		segmentsDrawer.draw();
		Graphics.CopyTexture( src: segmentsDrawer.renderedTexture, dst: grid_plot_texture );

		segmentsDrawer.mode = DrawSegments.Mode.Polyline;

		segmentsDrawer.segments = old_segments;

		valuePlotRawImage.texture = value_plot_texture;
		gridPlotRawImage .texture = grid_plot_texture;
	}
	private void check_initial_fill()
	{
		if ( startFilled )
		{
			for ( int i = 0; i != values.Length; ++i )
			{
				Vector2 v = startFillValue;
				if ( fillIndex )
				{
					startFillValue.x = i;
				}
				AddSample( startFillValue, false );
			}
			refreshPlotValues();
		}
	}
	private void update_ranges( Vector2 xy ) => update_ranges( xy.x, xy.y );
	private void update_ranges( float x, float y )
	{
		values_range_max.x = Mathf.Max( values_range_max.x, x );
		values_range_max.y = Mathf.Max( values_range_max.y, y );
		values_range_min.x = Mathf.Min( values_range_min.x, x );
		values_range_min.y = Mathf.Min( values_range_min.y, y );
	}
	#endregion private
}
