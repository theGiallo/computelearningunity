#if ( UNITY_IOS || UNITY_ANDROID ) && ! UNITY_EDITOR
#define MOBILE_SHADING
#endif
//#if false//! MOBILE_SHADING
//#define DRAW_INDIRECT
//#endif
#if true//MOBILE_SHADING
#define SDF_ENCODED_POS
#endif
#if true//MOBILE_SHADING
#define WPOSDIST_TEXTURE
#endif
#if true//MOBILE_SHADING
#define COMPUTE_POS_DIST_WHILE_DRAWING
#endif

using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;
using tg;

public class DrawSegments : MonoBehaviour
{
	[Serializable]
	[StructLayout(LayoutKind.Sequential, Pack = 0)]
	public struct Segment
	{
		public Vector2 start, end;
		public Segment( Vector2 start, Vector2 end )
		{
			this.start = start;
			this.end   = end;
		}

		public override string ToString()
		{
			return $"{start} -> {end}";
		}
	}
	public enum Mode
	{
		Segments,
		Polyline
	}
	public enum VertsGeneration
	{
		Compute,
		Shader,
		ShaderDraw,
		CPU
	}

	public Mode            mode = Mode.Segments;
	public VertsGeneration vertsGenMode = VertsGeneration.Compute;
	public bool            segmentsInTexture = false;
	public bool            twoTrisPerSegment = false;
	public bool            drawIndirect      = true;
	public ComputeShader   segsToTrisCompute;
	public Shader          segsToTrisShader;
	private Material       segsToTrisShaderMaterial;
	public Shader          sdfDrawShader;
	public Material        sdfShadingMaterial;
	public float           radius = 1;
	public List<Segment>   segments = new List<Segment>();
	public List<Vector2>   polyline = new List<Vector2>();
	public int width = 1024, height = 1024;
	public bool autoUpdate = false;
	public Texture renderedTexture => sdf_render_texture;
	public UnityEngine.UI.RawImage rawImage;

	[Header("Test")]
	[MinValue(0)]
	public int   testCircleSides    = 1024;
	[MinValue(0)]
	public float testCircleRadius   = 256;

	public void draw()
	{
		setup();
		if ( mode == Mode.Segments )
		{
			segments_to_triangles( segments, vertsGenMode );
		} else
		{
			polyline_to_triangles( polyline, vertsGenMode );
		}
		draw_sdf_triangles( vertsGenMode );
		if ( rawImage != null )
		{
			rawImage.texture = sdf_render_texture;
		}
	}

	#region Unity
	private void Start()
	{
		//test();
	}
	private void Update()
	{
		if ( autoUpdate )
		{
			draw();
		}

		//test();
		//if ( Input.GetKeyDown( KeyCode.Return ) )
		//{
		//	test();
		//}
	}
	private void OnDestroy()
	{
		destroy();
	}
	#endregion Unity

	#region private
	private const string COMPUTE_POS_DIST_KW      = "COMPUTE_POS_DIST";
	private const string POLYLINE_KW              = "POLYLINE";
	private const string SEGMENTS_KW              = "SEGMENTS";
	private const string SEGMENTS_IN_TEXTURE_KW   = "SEGMENTS_IN_TEXTURE";
	private const string TWO_TRIS_PER_SEGMENTS_KW = "TWO_TRIS_PER_SEGMENT";
	private const string VERTS_GEN_CPU_KW         = "VERTS_GEN_CPU";
	private const string INDIRECT_RENDERING_KW    = "INDIRECT_RENDERING";

	private GraphicsBuffer segments_buffer;
	private Texture2D[] segments_textures;
	private int[]       segments_textures_widths;

	private Texture2D curr_segments_texture;
	private int       curr_segments_texture_width;

	#if ! WPOSDIST_TEXTURE
	private GraphicsBuffer pos_dist_write_buffer;
	#endif
	private static GraphicsBuffer indexes_write_buffer;
	private static GraphicsBuffer indexes_write_buffer_2;
	#if WPOSDIST_TEXTURE
	private RenderTexture pos_dist_write_texture;
	#endif

	private static uint[] indexes;
	private static uint[] indexes2;

	private List<Vector2> distances     = new List<Vector2>();
	private List<Vector2> rel_positions = new List<Vector2>();
	private List<Vector2> ab_lengths    = new List<Vector2>();

	#if SDF_ENABLE_DEBUG_BUFFER
	private GraphicsBuffer debug_vertex_buffer;
	#endif

	private ComputeBuffer args_buffer;
	private int[] args = new int[] { 0, 1, 0, 0 };

	//#if ! DRAW_INDIRECT
	private Mesh stub_mesh;
	private List<Vector3> stub_vertices;
	private List<int>     stub_indexes;
	//#endif


	private CommandBuffer command_buffer;


	private int segs_to_tris_kernel_id     = -1;
	private int polyline_to_tris_kernel_id = -1;
	private int kernel_id => mode == Mode.Segments ? segs_to_tris_kernel_id : polyline_to_tris_kernel_id;

	private int segments_count_id        = -1;
	private int segments_buffer_id       = -1;
	private int segments_texture_id      = -1;
	private int segments_texture_width_id = -1;
	private int pos_dist_write_buffer_id = -1;
	private int indexes_write_buffer_id  = -1;
	private int radius_id                = -1;
	private int image_size_id            = -1;
	private int pos_dist_buf_id          = -1;
	private static int index_buf_id             = -1;
	private int pos_decode_matrix_id     = -1;
	private int pos_encode_matrix_id     = -1;
	private int debug_vertex_buf_id      = -1;

	private const int                     SEGMENT_SIZE = FLOAT_SIZE * 4;
	private const int MAX_SEGMENTS_COUNT           = 1 * 1024 * 1024 / 2;// / TRIANGLES_PER_SEGMENT;//65536;
	private const int TRIANGLES_PER_SEGMENT        = 12;
	private const int VERTICES_PER_SEGMENT         = 12;
	private const int TRIANGLES_PER_SEGMENT_2      = 2;
	private const int VERTICES_PER_SEGMENT_2       = 4;
	private const int MAX_VERTEX_COUNT_2           = MAX_SEGMENTS_COUNT  * VERTICES_PER_SEGMENT_2;
	private const int MAX_VERTEX_COUNT             = MAX_SEGMENTS_COUNT  * VERTICES_PER_SEGMENT;
	private const int MAX_TRIANGLES_COUNT          = MAX_SEGMENTS_COUNT  * TRIANGLES_PER_SEGMENT;
	private const int MAX_TRIANGLES_COUNT_2        = MAX_SEGMENTS_COUNT  * TRIANGLES_PER_SEGMENT_2;
	private const int MAX_TRIANGLES_VERTEXES_COUNT = MAX_TRIANGLES_COUNT * 3;
	private const int MAX_TRIANGLES_VERTEXES_COUNT_2 = MAX_TRIANGLES_COUNT_2 * 3;
	private const int FLOAT_SIZE = 4;
	private const int INT_SIZE   = 4;
	private const int vertex_count_per_instance_index = 0;
	private const int instance_count_index            = 1;
	private const int start_vertex_location_index     = 2;
	private const int start_instance_location_index   = 3;

	private Vector2[] points_buf = new Vector2[MAX_SEGMENTS_COUNT*2];//MAX_SEGMENTS_COUNT+1]; NOTE(theGiallo): this is for the texture. The buf covers the texture area.

	private Material sdfDrawMaterial;
	private RenderTexture sdf_render_texture;

	private int last_line_mode       = -1;
	private int last_vert_gen_mode   = -1;
	private int last_segs_in_texture = -1;
	private int last_draw_indirect   = -1;
	private int last_tris_per_seg    = -1;
	private int triangles_per_segment => twoTrisPerSegment ? TRIANGLES_PER_SEGMENT_2 : TRIANGLES_PER_SEGMENT;
	private int vertices_per_segment  => twoTrisPerSegment ? VERTICES_PER_SEGMENT_2  : VERTICES_PER_SEGMENT;

	//private MaterialPropertyBlock sdfShadingMaterialMPB;

	private void setup()
	{
		if ( SystemInfo.graphicsDeviceType == GraphicsDeviceType.OpenGLES3 )
		{
			segmentsInTexture = true;
		}

		if ( Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer )
		{
			vertsGenMode = VertsGeneration.CPU;
		}

		if ( vertsGenMode == VertsGeneration.CPU )
		{
			drawIndirect = false;
		}

		if ( twoTrisPerSegment && vertsGenMode != VertsGeneration.CPU )
		{
			vertsGenMode = VertsGeneration.ShaderDraw;
		}

		if ( segments_buffer      == null ) segments_buffer      = new GraphicsBuffer( GraphicsBuffer.Target.Structured /*Constant*/, MAX_SEGMENTS_COUNT * 2, FLOAT_SIZE * 2 );

		if ( segments_textures == null )
		{
			int count = 6;
			segments_textures        = new Texture2D[count];
			segments_textures_widths = new int      [count];
			for ( int i = 0; i != count; ++i )
			{
				int side = 1 << ( i + 5 );
				segments_textures[i] = new Texture2D( side, side, TextureFormat.RGFloat, mipChain: false, linear: true );
				segments_textures_widths[i] = side;
			}
		}

		#if WPOSDIST_TEXTURE
		if ( pos_dist_write_texture == null )
		{
			pos_dist_write_texture = new RenderTexture( 1024, 1024, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear );
			pos_dist_write_texture.enableRandomWrite = true;
		}
		#elif WPOSDIST_F4
		if ( pos_dist_write_buffer == null ) pos_dist_write_buffer = new GraphicsBuffer( GraphicsBuffer.Target.Structured /*Vertex*/  , MAX_VERTEX_COUNT        , FLOAT_SIZE * 4 );
		#else
		if ( pos_dist_write_buffer == null ) pos_dist_write_buffer = new GraphicsBuffer( GraphicsBuffer.Target.Structured /*Vertex*/  , MAX_VERTEX_COUNT * 3        , FLOAT_SIZE );
		#endif

		if ( indexes_write_buffer  == null )
		{
			indexes_write_buffer  = new GraphicsBuffer( GraphicsBuffer.Target.Structured /*Index */  , MAX_TRIANGLES_VERTEXES_COUNT, INT_SIZE );
			gen_indexes();
		}
		if ( indexes_write_buffer_2  == null )
		{
			indexes_write_buffer_2  = new GraphicsBuffer( GraphicsBuffer.Target.Structured /*Index */  , MAX_TRIANGLES_VERTEXES_COUNT_2, INT_SIZE );
			gen_indexes_two_tris_per_segment();
		}
		#if SDF_ENABLE_DEBUG_BUFFER
		if ( debug_vertex_buffer  == null ) debug_vertex_buffer  = new GraphicsBuffer( GraphicsBuffer.Target.Structured | GraphicsBuffer.Target.Raw /*Index */  , MAX_TRIANGLES_VERTEXES_COUNT, FLOAT_SIZE * 4 );
		#endif

		if ( args_buffer          == null ) args_buffer          = new ComputeBuffer( 1, INT_SIZE * 4, ComputeBufferType.IndirectArguments );

		if ( points_buf == null ) points_buf = new Vector2[MAX_SEGMENTS_COUNT*2];
		segments.Capacity = MAX_SEGMENTS_COUNT;
		polyline.Capacity = points_buf.Length;

		if ( segments_buffer_id       == -1 ) segments_buffer_id       = Shader.PropertyToID( "read_segments"        );
		if ( segments_texture_id      == -1 ) segments_texture_id      = Shader.PropertyToID( "read_segments_tex"  );
		if ( segments_texture_width_id == -1 ) segments_texture_width_id = Shader.PropertyToID( "read_segments_tex_width"  );
		if ( pos_dist_write_buffer_id == -1 ) pos_dist_write_buffer_id = Shader.PropertyToID( "write_pos_dist"       );
		if ( indexes_write_buffer_id  == -1 ) indexes_write_buffer_id  = Shader.PropertyToID( "write_indexes"        );
		if ( radius_id                == -1 ) radius_id                = Shader.PropertyToID( "radius"               );
		if ( image_size_id            == -1 ) image_size_id            = Shader.PropertyToID( "image_size"           );
		if ( pos_dist_buf_id          == -1 ) pos_dist_buf_id          = Shader.PropertyToID( "_PosDistBuf"          );
		if ( index_buf_id             == -1 ) index_buf_id             = Shader.PropertyToID( "_IndexBuf"            );
		if ( pos_encode_matrix_id     == -1 ) pos_encode_matrix_id     = Shader.PropertyToID( "pos_encode_matrix"    );
		if ( pos_decode_matrix_id     == -1 ) pos_decode_matrix_id     = Shader.PropertyToID( "pos_decode_matrix"    );

		if ( segments_count_id        == -1 ) segments_count_id        = Shader.PropertyToID( "segments_count" );

		if ( segs_to_tris_kernel_id     == - 1 ) segs_to_tris_kernel_id     = segsToTrisCompute.FindKernel( "CSMain" );
		if ( polyline_to_tris_kernel_id == - 1 ) polyline_to_tris_kernel_id = segsToTrisCompute.FindKernel( "CSMain" );

		if ( sdfDrawMaterial          == null ) sdfDrawMaterial          = new Material( sdfDrawShader );
		if ( segsToTrisShaderMaterial == null ) segsToTrisShaderMaterial = new Material( segsToTrisShader );
		const int depth_bits =
			#if MOBILE_SHADING
			16
			#else
			24
			#endif
		;
		if ( sdf_render_texture       == null ) sdf_render_texture       = new RenderTexture( width, height, depth: depth_bits, RenderTextureFormat.ARGB32 );
		//if ( sdfShadingMaterialMPB == null ) sdfShadingMaterialMPB = new MaterialPropertyBlock();

		if ( command_buffer == null ) command_buffer = new CommandBuffer();

		//#if ! DRAW_INDIRECT
		if ( stub_mesh     == null ) stub_mesh     = new Mesh();
		if ( stub_vertices == null ) stub_vertices = new List<Vector3>();
		if ( stub_indexes  == null ) stub_indexes  = new List<int>();
		//#endif
	}
	private void gen_indexes()
	{
		indexes = new uint[MAX_TRIANGLES_VERTEXES_COUNT];
		for ( int i = 0; i != MAX_SEGMENTS_COUNT; ++i )
		{
			uint idx_start = (uint)(i * VERTICES_PER_SEGMENT);
			uint triangles_idx_start = (uint)(i * TRIANGLES_PER_SEGMENT * 3);

			uint A_idx =  0 + idx_start;
			uint B_idx =  1 + idx_start;
			uint C_idx =  2 + idx_start;
			uint D_idx =  3 + idx_start;
			uint E_idx =  4 + idx_start;
			uint F_idx =  5 + idx_start;
			uint G_idx =  6 + idx_start;
			uint H_idx =  7 + idx_start;
			uint I_idx =  8 + idx_start;
			uint J_idx =  9 + idx_start;
			uint K_idx = 10 + idx_start;
			uint L_idx = 11 + idx_start;

			int ti = 0;
			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = E_idx;
			indexes[triangles_idx_start + ti++] = C_idx;

			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = F_idx;
			indexes[triangles_idx_start + ti++] = E_idx;

			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = G_idx;
			indexes[triangles_idx_start + ti++] = F_idx;

			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = B_idx;
			indexes[triangles_idx_start + ti++] = G_idx;

			indexes[triangles_idx_start + ti++] = B_idx;
			indexes[triangles_idx_start + ti++] = H_idx;
			indexes[triangles_idx_start + ti++] = G_idx;

			indexes[triangles_idx_start + ti++] = B_idx;
			indexes[triangles_idx_start + ti++] = D_idx;
			indexes[triangles_idx_start + ti++] = H_idx;

			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = C_idx;
			indexes[triangles_idx_start + ti++] = I_idx;

			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = I_idx;
			indexes[triangles_idx_start + ti++] = J_idx;

			indexes[triangles_idx_start + ti++] = A_idx;
			indexes[triangles_idx_start + ti++] = J_idx;
			indexes[triangles_idx_start + ti++] = B_idx;

			indexes[triangles_idx_start + ti++] = B_idx;
			indexes[triangles_idx_start + ti++] = J_idx;
			indexes[triangles_idx_start + ti++] = K_idx;

			indexes[triangles_idx_start + ti++] = B_idx;
			indexes[triangles_idx_start + ti++] = K_idx;
			indexes[triangles_idx_start + ti++] = L_idx;

			indexes[triangles_idx_start + ti++] = B_idx;
			indexes[triangles_idx_start + ti++] = L_idx;
			indexes[triangles_idx_start + ti++] = D_idx;
		}
		indexes_write_buffer.SetData( indexes );
	}

	private void gen_indexes_two_tris_per_segment()
	{
		indexes2 = new uint[MAX_TRIANGLES_VERTEXES_COUNT_2];
		for ( int i = 0; i != MAX_SEGMENTS_COUNT; ++i )
		{
			uint idx_start = (uint)(i * VERTICES_PER_SEGMENT_2);
			uint triangles_idx_start = (uint)(i * TRIANGLES_PER_SEGMENT_2 * 3);
			/*
				E----------------------H
				|                    ..|
				|                 ...  |
				|               ..     |
				|            ...       |
				-----A---- .. ----B-----
				|       ...            |
				|     ..               |
				|  ...                 |
				|..                    |
				I----------------------L

				Two triangles version.

			*/

			uint E_idx =  0 + idx_start;
			uint H_idx =  1 + idx_start;
			uint I_idx =  2 + idx_start;
			uint L_idx =  3 + idx_start;

			int ti = 0;
			indexes2[triangles_idx_start + ti++] = E_idx;
			indexes2[triangles_idx_start + ti++] = I_idx;
			indexes2[triangles_idx_start + ti++] = H_idx;

			indexes2[triangles_idx_start + ti++] = H_idx;
			indexes2[triangles_idx_start + ti++] = I_idx;
			indexes2[triangles_idx_start + ti++] = L_idx;
		}
		indexes_write_buffer_2.SetData( indexes2 );
	}
	[Button]
	private void destroy()
	{
		if ( segments_buffer       != null ) { segments_buffer      .Release(); segments_buffer       = null; }
		if ( segments_textures != null )
		{
			for ( int i = 0; i != segments_textures.Length; ++i )
			{
				Destroy( segments_textures[i] );
			}
			segments_textures           = null;
			segments_textures_widths    = null;
			curr_segments_texture       = null;
			curr_segments_texture_width = 0;
		}

		#if WPOSDIST_TEXTURE
		if ( pos_dist_write_texture     != null ) { pos_dist_write_texture    .Release(); pos_dist_write_texture     = null; }
		#else
		if ( pos_dist_write_buffer != null ) { pos_dist_write_buffer.Release(); pos_dist_write_buffer = null; }
		#endif

		if ( indexes_write_buffer   != null ) { indexes_write_buffer  .Release(); indexes_write_buffer   = null; }
		if ( indexes_write_buffer_2 != null ) { indexes_write_buffer_2.Release(); indexes_write_buffer_2 = null; }

		#if SDF_ENABLE_DEBUG_BUFFER
		if ( debug_vertex_buffer   != null ) { debug_vertex_buffer  .Release(); debug_vertex_buffer   = null; }
		#endif

		if ( args_buffer           != null ) { args_buffer          .Release(); args_buffer           = null; }
		if ( command_buffer        != null ) { command_buffer       .Release(); command_buffer        = null; }
		if ( sdf_render_texture    != null ) { sdf_render_texture   .Release(); sdf_render_texture    = null; }

		points_buf = null;

		segments_buffer_id         = -1;
		segments_texture_id        = -1;
		segments_texture_width_id  = -1;
		pos_dist_write_buffer_id   = -1;
		indexes_write_buffer_id    = -1;
		radius_id                  = -1;
		image_size_id              = -1;
		pos_dist_buf_id            = -1;
		index_buf_id               = -1;
		pos_encode_matrix_id       = -1;
		pos_decode_matrix_id       = -1;

		segments_count_id          = -1;

		segs_to_tris_kernel_id     = -1;
		polyline_to_tris_kernel_id = -1;

		command_buffer = null;

		//#if ! DRAW_INDIRECT
		stub_mesh     = null;
		stub_vertices = null;
		stub_indexes  = null;
		//#endif
	}

	unsafe private void segments_to_triangles( List<Segment> segments, VertsGeneration verts_gen_mode  )
	{
		int points_count = Mathf.Min( segments.Count * 2, points_buf.Length );
		/*
		for ( int i = 0, pi = 0; pi != points_count; ++i, pi += 2 )
		{
			points_buf[pi + 0] = segments[i].start;
			points_buf[pi + 1] = segments[i].end;
		}
		*/

		Type lt = typeof( List<Segment> );
		var f = lt.GetField( "_items", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance );
		var segments_arr = (Segment[])f.GetValue( segments );

		fixed ( Segment * segments_ptr = segments_arr )
		{
			gen_tris( (Vector2*)segments_ptr, segments_arr, points_count, Mode.Segments, verts_gen_mode );
		}
	}
	unsafe private void polyline_to_triangles( List<Vector2> polyline, VertsGeneration verts_gen_mode )
	{
		int points_count = Mathf.Min( polyline.Count, points_buf.Length );
		/*
		for ( int i = 0; i != points_count; ++i )
		{
			points_buf[i] = polyline[i];
		}
		*/

		Type lt = typeof( List<Vector2> );
		var f = lt.GetField( "_items", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance );
		var polyline_arr = (Vector2[])f.GetValue( polyline );

		fixed ( Vector2 * points_ptr = polyline_arr )
		{
			gen_tris( (Vector2*)points_ptr, polyline_arr, points_count, Mode.Polyline, verts_gen_mode );
		}
		//gen_tris( points_buf, count, Mode.Polyline, verts_gen_mode );
	}
	unsafe private void gen_tris( Vector2 * points_ptr, Array points, int points_count, Mode mode, VertsGeneration verts_gen_mode )
	{
		setup_keywords( mode, verts_gen_mode, segmentsInTexture );
		int segments_count = upload_segments_buf( points_ptr, points, points_count );
		switch ( verts_gen_mode )
		{
			case VertsGeneration.Compute:
			generate_triangles( segments_count, mode );
			break;
			case VertsGeneration.Shader:
			generate_triangles_shader_setup( segments_count, mode );
			break;
			case VertsGeneration.ShaderDraw:
			break;
			case VertsGeneration.CPU:
			break;
		}
	}
	private void setup_keywords( Mode mode, VertsGeneration verts_gen_mode, bool segmentsInTexture )
	{
		if ( last_line_mode       != (int)mode
		  || last_vert_gen_mode   != (int)verts_gen_mode
		  || last_segs_in_texture != ( segmentsInTexture ? 1 : 0 )
		  || last_draw_indirect   != ( drawIndirect ? 1 : 0 )
		  || last_tris_per_seg    != triangles_per_segment
		)
		{
			if ( mode == Mode.Segments )
			{
				switch ( verts_gen_mode )
				{
					case VertsGeneration.Compute:
					segsToTrisCompute.DisableKeyword( POLYLINE_KW );
					segsToTrisCompute.EnableKeyword( SEGMENTS_KW );
					break;
					case VertsGeneration.Shader:
					segsToTrisShaderMaterial.DisableKeyword( POLYLINE_KW );
					segsToTrisShaderMaterial.EnableKeyword( SEGMENTS_KW );
					break;
					case VertsGeneration.ShaderDraw:
					sdfDrawMaterial.DisableKeyword( POLYLINE_KW );
					sdfDrawMaterial.EnableKeyword( SEGMENTS_KW );
					break;
				}
			} else
			{
				switch ( verts_gen_mode )
				{
					case VertsGeneration.Compute:
					segsToTrisCompute.EnableKeyword( POLYLINE_KW );
					segsToTrisCompute.DisableKeyword( SEGMENTS_KW );
					break;
					case VertsGeneration.Shader:
					segsToTrisShaderMaterial.EnableKeyword( POLYLINE_KW );
					segsToTrisShaderMaterial.DisableKeyword( SEGMENTS_KW );
					break;
					case VertsGeneration.ShaderDraw:
					sdfDrawMaterial.EnableKeyword( POLYLINE_KW );
					sdfDrawMaterial.DisableKeyword( SEGMENTS_KW );
					break;
				}
			}

			switch ( verts_gen_mode )
			{
				case VertsGeneration.Compute:
				sdfDrawMaterial.DisableKeyword( VERTS_GEN_CPU_KW );
				sdfDrawMaterial.DisableKeyword( TWO_TRIS_PER_SEGMENTS_KW );
				sdfDrawMaterial.DisableKeyword( COMPUTE_POS_DIST_KW );
				if ( segmentsInTexture )
				{
					segsToTrisCompute.EnableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				else
				{
					segsToTrisCompute.DisableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				break;

				case VertsGeneration.Shader:
				sdfDrawMaterial.DisableKeyword( VERTS_GEN_CPU_KW );
				sdfDrawMaterial.DisableKeyword( TWO_TRIS_PER_SEGMENTS_KW );
				sdfDrawMaterial.DisableKeyword( COMPUTE_POS_DIST_KW );
				if ( segmentsInTexture )
				{
					segsToTrisShaderMaterial.EnableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				else
				{
					segsToTrisShaderMaterial.DisableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				break;

				case VertsGeneration.ShaderDraw:
				sdfDrawMaterial.DisableKeyword( VERTS_GEN_CPU_KW );
				if ( twoTrisPerSegment )
				{
					sdfDrawMaterial.EnableKeyword( TWO_TRIS_PER_SEGMENTS_KW );
				} else
				{
					sdfDrawMaterial.DisableKeyword( TWO_TRIS_PER_SEGMENTS_KW );
				}
				sdfDrawMaterial.EnableKeyword( COMPUTE_POS_DIST_KW );
				if ( segmentsInTexture )
				{
					sdfDrawMaterial.EnableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				else
				{
					sdfDrawMaterial.DisableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				break;

				case VertsGeneration.CPU:
				sdfDrawMaterial.EnableKeyword( VERTS_GEN_CPU_KW );
				if ( twoTrisPerSegment )
				{
					sdfDrawMaterial.EnableKeyword( TWO_TRIS_PER_SEGMENTS_KW );
				} else
				{
					sdfDrawMaterial.DisableKeyword( TWO_TRIS_PER_SEGMENTS_KW );
				}
				sdfDrawMaterial.EnableKeyword( COMPUTE_POS_DIST_KW );
				if ( segmentsInTexture )
				{
					sdfDrawMaterial.EnableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				else
				{
					sdfDrawMaterial.DisableKeyword( SEGMENTS_IN_TEXTURE_KW );
				}
				break;


				break;
			}

			if ( last_draw_indirect != ( drawIndirect ? 1 : 0 ) )
			{
				if ( drawIndirect )
				{
					sdfDrawMaterial.EnableKeyword( INDIRECT_RENDERING_KW );
				} else
				{
					sdfDrawMaterial.DisableKeyword( INDIRECT_RENDERING_KW );
				}
			}

			last_line_mode       = (int)mode;
			last_vert_gen_mode   = (int)verts_gen_mode;
			last_segs_in_texture = segmentsInTexture ? 1 : 0;
			last_draw_indirect   = drawIndirect ? 1 : 0;
			last_tris_per_seg    = triangles_per_segment;
		}
	}
	private void select_texture_for( int points_count )
	{
		int chosen_w = int.MaxValue;
		int chosen_index = -1;
		for ( int i = 0; i != segments_textures_widths.Length; ++i )
		{
			int w = segments_textures_widths[i];
			int area = w * w;
			if ( area >= points_count && w < chosen_w )
			{
				chosen_w = w;
				chosen_index = i;
			}
		}
		if ( chosen_index != -1 )
		{
			curr_segments_texture       = segments_textures[chosen_index];
			curr_segments_texture_width = chosen_w;
		}
		else
		{
			curr_segments_texture       = null;
			curr_segments_texture_width = 0;
		}
	}

	unsafe private int upload_segments_buf( Vector2 * points_ptr, Array points, int points_count )
	{
		int segments_count = mode == Mode.Segments ? points_count / 2 : points_count - 1;
		if ( segments_count <= 0 )
		{
			return segments_count;
		}

		if ( segmentsInTexture )
		{
			select_texture_for( points_count );
			int w = curr_segments_texture_width;
			int area = w * w;
			curr_segments_texture.LoadRawTextureData( (IntPtr)points_ptr, area * FLOAT_SIZE * 2 );
			curr_segments_texture.Apply();
			//for ( int y = 0; y != 1024; ++y )
			//for ( int x = 0; x != 1024; ++x )
			//{
			//	var v = arr[y * 1024 + x];
			//	Color color = new Color( v.x, v.y, 0, 0 );
			//	segments_texture.SetPixel( x, y, color );
			//}
		} else
		{
			segments_buffer.SetData( points, 0, 0, points_count );
			if ( ! segments_buffer.IsValid() )
			{
				Debug.LogError( "segments buffer not valid" );
			}
		}

		return segments_count;
	}

	private void generate_triangles( int segments_count, Mode mode )
	{
		if ( segments_count <= 0 )
		{
			return;
		}

		int _kernel_id = mode == Mode.Segments ? segs_to_tris_kernel_id : polyline_to_tris_kernel_id;

		if ( segmentsInTexture )
		{
			segsToTrisCompute.SetTexture( _kernel_id, segments_texture_id,       curr_segments_texture );
			segsToTrisCompute.SetInt    ( segments_texture_width_id, curr_segments_texture_width );
		}
		else
		{
			segsToTrisCompute.SetBuffer( _kernel_id, segments_buffer_id, segments_buffer );//, 0, segments.Count * SEGMENT_SIZE );
		}

		#if SDF_ENCODED_POS
		segsToTrisCompute.SetMatrix( pos_encode_matrix_id, pos_encode_matrix );
		#endif

		#if WPOSDIST_TEXTURE
		segsToTrisCompute.SetTexture( _kernel_id, pos_dist_write_buffer_id, pos_dist_write_texture );
		#else
		segsToTrisCompute.SetBuffer( _kernel_id, pos_dist_write_buffer_id, pos_dist_write_buffer );
		#endif
		//#endif
		if ( twoTrisPerSegment )
		{
			segsToTrisCompute.SetBuffer( _kernel_id, indexes_write_buffer_id,  indexes_write_buffer_2 );
		} else
		{
			segsToTrisCompute.SetBuffer( _kernel_id, indexes_write_buffer_id,  indexes_write_buffer   );
		}

		segsToTrisCompute.SetFloat( radius_id, radius );

		//Debug.Log( $"array.Length: {array.Length}" );
		//Debug.Log( $"array_length: {array_length}" );
		//Debug.Log( $"segments_count: {segments_count}" );

		segsToTrisCompute.SetInt( segments_count_id, segments_count );

		segsToTrisCompute.GetKernelThreadGroupSizes( _kernel_id,
		                                         out uint thread_group_size_x,
		                                         out uint thread_group_size_y,
		                                         out uint thread_group_size_z );
		int threadGroupsX = ( segments_count + (int)thread_group_size_x - 1 ) / (int)thread_group_size_x;
		int threadGroupsY = ( 1 + (int)thread_group_size_y - 1 ) / (int)thread_group_size_y;
		int threadGroupsZ = ( 1 + (int)thread_group_size_z - 1 ) / (int)thread_group_size_z;
		//Debug.Log( $"thread group size: {thread_group_size_x}x{thread_group_size_y}x{thread_group_size_z} ({thread_group_size_x*thread_group_size_y*thread_group_size_z})" );
		//Debug.Log( $"thread groups: {threadGroupsX}x{threadGroupsY}x{threadGroupsZ} ({threadGroupsX*threadGroupsY*threadGroupsZ})" );

		segsToTrisCompute.Dispatch( _kernel_id, threadGroupsX: threadGroupsX, threadGroupsY: threadGroupsY, threadGroupsZ: threadGroupsZ );
	}
	private void generate_triangles_shader_setup( int segments_count, Mode mode )
	{
		if ( segments_count <= 0 )
		{
			return;
		}

		//int _kernel_id = mode == Mode.Segments ? segs_to_tris_kernel_id : polyline_to_tris_kernel_id;

		if ( segmentsInTexture )
		{
			segsToTrisShaderMaterial.SetTexture( segments_texture_id, curr_segments_texture );
			segsToTrisShaderMaterial.SetInt    ( segments_texture_width_id, curr_segments_texture_width );
		}
		else
		{
			segsToTrisShaderMaterial.SetBuffer( segments_buffer_id, segments_buffer );
		}

		#if SDF_ENCODED_POS
		segsToTrisShaderMaterial.SetMatrix( pos_encode_matrix_id, pos_encode_matrix );
		#endif

		segsToTrisShaderMaterial.SetFloat( radius_id, radius );
		segsToTrisShaderMaterial.SetVector( image_size_id, new Vector4(1024,1024,0,0) );

		//Debug.Log( $"array.Length: {array.Length}" );
		//Debug.Log( $"array_length: {array_length}" );
		//Debug.Log( $"segments_count: {segments_count}" );

		segsToTrisShaderMaterial.SetInt( segments_count_id, segments_count );
	}
	Vector2 read_segment( int segment_index, int ab01 )
	{
		Vector2 ret = default;
		switch ( mode )
		{
			case Mode.Segments:
			{
				var s = segments[segment_index];
				ret = ab01 == 0 ? s.start : s.end;
			}
			break;
			case Mode.Polyline:
				ret = polyline[segment_index + ab01];
			break;
		}
		return ret;
	}
	bool compute_vertex_2( int segments_count, int index, out Vector2 pos, out Vector2 rel_pos, out float ab_len )
	{
		int verts_per_segment = vertices_per_segment;
		int segment_index = index / verts_per_segment;
		if ( segment_index >= segments_count )
		{
			pos = new Vector2(0,0);
			rel_pos = pos;
			ab_len = 0;
			return false;
		}
		int v_index_seg_rel = index - segment_index * verts_per_segment;

		Vector2 A = read_segment( segment_index, 0 );
		Vector2 B = read_segment( segment_index, 1 );

		Vector2 vec   = B - A;
		float  _ab_len = vec.magnitude;
		Vector2 fw    = _ab_len == 0 ? vec : vec / _ab_len;
		Vector2 right = new Vector2(fw.y, -fw.x);
		Vector2 rr = right * radius;
		Vector2 fr = fw * radius;

		Vector2[] verts = new Vector2[]{
		/* E */ A - fr - rr,
		/* H */ B + fr - rr,
		/* I */ A - fr + rr,
		/* L */ B + fr + rr,
		};

		Vector2 _pos = new Vector2(0,0);
		_pos  = verts[v_index_seg_rel];

		pos  = _pos;

		Vector2 _rel_p = _pos - A;
		float x = Vector2.Dot( _rel_p, fw );
		float y = Vector2.Dot( _rel_p, -right );
		rel_pos = new Vector2( x, y );
		ab_len  = _ab_len;

		return true;
	}
	bool compute_vertex( int segments_count, int index, out Vector2 pos, out float dist )
	{
		int verts_per_segment = vertices_per_segment;
		int segment_index = index / verts_per_segment;
		if ( segment_index >= segments_count )
		{
			pos = new Vector2(0,0);
			dist = 0;
			return false;
		}
		int v_index_seg_rel = index - segment_index * verts_per_segment;

		Vector2 A = read_segment( segment_index, 0 );
		Vector2 B = read_segment( segment_index, 1 );

		Vector2 vec   = B - A;
		float  _ab_len = vec.magnitude;
		Vector2 fw    = _ab_len == 0 ? vec : vec / _ab_len;
		Vector2 right = new Vector2(fw.y, -fw.x);
		Vector2 rr = right * radius;
		Vector2 fr = fw * radius;
		const float SQRT2 = 1.41421356237309504880f;
		float dist_far = SQRT2 * radius;


		Vector2[] verts = new Vector2[]{
		/* A */ A          ,
		/* B */ B          ,
		/* C */ A - fr     ,
		/* D */ B + fr     ,
		/* E */ A - fr - rr,
		/* F */ A - rr     ,
		/* G */ B - rr     ,
		/* H */ B + fr - rr,
		/* I */ A - fr + rr,
		/* J */ A + rr     ,
		/* K */ B + rr     ,
		/* L */ B + fr + rr,
		};

		float[] dists = new float[]{
		/* A */ 0       ,
		/* B */ 0       ,
		/* C */ radius  ,
		/* D */ radius  ,
		/* E */ dist_far,
		/* F */ radius  ,
		/* G */ radius  ,
		/* H */ dist_far,
		/* I */ dist_far,
		/* J */ radius  ,
		/* K */ radius  ,
		/* L */ dist_far,
		};

		Vector2 _pos = new Vector2(0,0);
		float _dist = 0;

		_pos  = verts[v_index_seg_rel];
		_dist = dists[v_index_seg_rel];

		_dist /= radius;

		pos  = _pos;
		dist = _dist;

		return true;
	}

	private void draw_sdf_triangles( VertsGeneration verts_gen_mode )
	{
		int segments_count = mode == Mode.Segments ? segments.Count : polyline.Count - 1;
		//if ( segments_count == 0 )
		//{
		//	return;
		//}

		// Indirect args just stores the number of verts for the draw call
		/*
			int vertexCountPerInstance;
			int instanceCount;
			int startVertexLocation;
			int startInstanceLocation;
		 */
		int start_instance_location   = args[start_instance_location_index  ] = 0;
		int start_vertex_location     = args[start_vertex_location_index    ] = 0;
		int instance_count            = args[instance_count_index           ] = 1;
		int vertex_count_per_instance = args[vertex_count_per_instance_index] = 3 * triangles_per_segment * segments_count;
		//#if DRAW_INDIRECT
		if ( drawIndirect )
		{
			args_buffer.SetData( args );
		}
		//#endif

		Bounds bounds = new Bounds( center: Vector3.zero, size: Vector3.one );
		MeshTopology topology = MeshTopology.Triangles;

		float left, right, bottom, top, zNear, zFar;

		#if false//SDF_ENCODED_POS
		left   = 0;
		right  = 1;
		bottom = 0;
		top    = 1;
		#else
		left   = 0;
		right  = width;
		bottom = 0;
		top    = height;
		#endif

		zNear  = -1;
		zFar   = 4096;

		Matrix4x4 proj_mx = Matrix4x4.Ortho( left: left, right: right, bottom: bottom, top: top, zNear: zNear, zFar: zFar );
		// The returned matrix embeds a z-flip operation whose purpose is to cancel the z-flip performed by the camera view matrix.
		// If the view matrix is an identity or some custom matrix that doesn't perform a z-flip, consider multiplying the third
		// column of the projection matrix (i.e. m02, m12, m22 and m32) by -1.
		//proj_mx.m02 *= -1;
		//proj_mx.m12 *= -1;
		//proj_mx.m22 *= -1;
		//proj_mx.m32 *= -1;
		//proj_mx = Matrix4x4.TRS( new Vector3( -1, -1, 0 ), Quaternion.identity,  new Vector3( 2f / (float)width, 2f / (float)height, 1 ) );
		//proj_mx = Matrix4x4.TRS( new Vector3( 0, 0, 0 ), Quaternion.identity,  new Vector3( 1f / (float)width, 1f / (float)height, 1 ) );
		//Debug.Log( $"proj_mx:\n{proj_mx}" );

		Matrix4x4 view_mx  = Matrix4x4.identity;
		Matrix4x4 model_mx = Matrix4x4.identity;
		//proj_mx = GL.GetGPUProjectionMatrix( proj_mx, renderIntoTexture: true );

		#if SDF_ENCODED_POS
		if ( verts_gen_mode != VertsGeneration.ShaderDraw )
		{
			sdfDrawMaterial.SetMatrix( pos_decode_matrix_id, pos_decode_matrix );
		}
		#endif

		if ( verts_gen_mode == VertsGeneration.ShaderDraw )
		{
			sdfDrawMaterial.SetInt( segments_count_id, segments_count );

			if ( segmentsInTexture )
			{
				sdfDrawMaterial.SetTexture( segments_texture_id, curr_segments_texture );
				sdfDrawMaterial.SetInt    ( segments_texture_width_id, curr_segments_texture_width );
			}
			else
			{
				sdfDrawMaterial.SetBuffer( segments_buffer_id, segments_buffer );
			}
		} else
		{
			#if WPOSDIST_TEXTURE
			sdfDrawMaterial.SetTexture( pos_dist_buf_id,   pos_dist_write_texture     );
			#else
			sdfDrawMaterial.SetBuffer( pos_dist_buf_id, pos_dist_write_buffer );
			#endif
		}

		if ( twoTrisPerSegment && vertsGenMode != VertsGeneration.CPU )
		{
			sdfDrawMaterial.SetBuffer( index_buf_id,    indexes_write_buffer_2 );
		}

		if ( ! twoTrisPerSegment )
		{
			sdfDrawMaterial.SetBuffer( index_buf_id,    indexes_write_buffer   );
		}

		#if SDF_ENABLE_DEBUG_BUFFER
		//Graphics.SetRandomWriteTarget( 7, debug_vertex_buffer );
		sdfDrawMaterial.SetBuffer( debug_vertex_buf_id, debug_vertex_buffer );
		#endif

		Rect viewport_px = new Rect( 0, 0, width, height );

		command_buffer.Clear();

		if ( verts_gen_mode == VertsGeneration.Shader )
		{
			command_buffer.Blit( source: null, dest: pos_dist_write_texture, segsToTrisShaderMaterial );
		}

		command_buffer.SetRenderTarget( sdf_render_texture );
		command_buffer.ClearRenderTarget( true, true, Color.red, 1 );

		viewport_px = new Rect( 0, 0, width, height );
		command_buffer.SetViewport( viewport_px );
		command_buffer.SetViewProjectionMatrices( view_mx, proj_mx );

		#if SDF_ENABLE_DEBUG_BUFFER
		command_buffer.SetRandomWriteTarget( 7, debug_vertex_buffer );
		#endif

		sdfDrawMaterial.SetFloat( radius_id, radius );

		if ( segments_count > 0 )
		{
		//#if DRAW_INDIRECT
		if ( drawIndirect )
		{
			//command_buffer.DrawProceduralIndirect( model_mx, sdfDrawMaterial, shaderPass:0, topology, args_buffer, argsOffset: 0, properties: null );
			command_buffer.DrawProcedural( model_mx,
			                               sdfDrawMaterial,
			                               shaderPass:0,
			                               topology,
			                               vertexCount: vertex_count_per_instance,
			                               instanceCount: instance_count,
			                               properties: null );
		//#else
		} else
		{
			int indexes_count  = ( triangles_per_segment * 3 ) * segments_count;
			// NOTE(theGiallo): this seems to make it work
			int vertices_count;// = indexes_count;
			//int vertices_count = vertices_per_segment * segments_count;
			//if ( vertsGenMode == VertsGeneration.CPU )
			{
				vertices_count = vertices_per_segment * segments_count;
			}

			bool updated = false;

			if ( stub_vertices.Count != vertices_count
			  || vertsGenMode == VertsGeneration.CPU )
			{
				//Debug.Log( $"stub_vertices.Count: {stub_vertices.Count} vertices count: {vertices_count}" );
				updated = true;
				if ( vertsGenMode == VertsGeneration.CPU )
				{
					rel_positions.Clear();
					ab_lengths   .Clear();
					stub_vertices.Clear();
					distances    .Clear();

					if ( twoTrisPerSegment )
					{
						for ( int i = stub_vertices.Count; i < vertices_count; ++i )
						{
							compute_vertex_2( segments_count, i, out Vector2 pos, out Vector2 rel_pos, out float ab_len );
							rel_positions.Add( rel_pos );
							ab_lengths   .Add( new Vector2( ab_len, 0 ) );
							stub_vertices.Add( new Vector3( pos.x, pos.y, 0 ) );
						}
					} else
					{
						for ( int i = stub_vertices.Count; i < vertices_count; ++i )
						{
							compute_vertex( segments_count, i, out Vector2 pos, out float depth );
							distances    .Add( new Vector2( depth, 0 ) );
							stub_vertices.Add( new Vector3( pos.x, pos.y, 0 ) );
						}
					}
				} else
				{
					if ( stub_vertices.Count < vertices_count )
					{
						for ( int i = stub_vertices.Count; i < vertices_count; ++i )
						{
							stub_vertices.Add( new Vector3( ( i % 2 ) / 2f, ( 1 - ( i % 2 ) ) / 2f, 0 ) );
						}
					} else
					{
						while ( stub_vertices.Count > vertices_count )
						{
							stub_vertices.RemoveAt( stub_vertices.Count - 1 );
						}
					}
				}
			}

			if ( stub_indexes.Count != indexes_count )
			{
				//Debug.Log( $"1 stub_indexes.Count: {stub_indexes.Count} indexes count: {indexes_count}" );
				stub_indexes.Clear();
				//Debug.Log( $"2 stub_indexes.Count: {stub_indexes.Count} indexes count: {indexes_count}" );
				updated = true;
				if ( twoTrisPerSegment )
				{
					for ( int i = stub_indexes.Count; i < indexes_count; ++i )
					{
						stub_indexes.Add( (int)indexes2[i] );
					}
				} else
				{
					for ( int i = stub_indexes.Count; i < indexes_count; ++i )
					{
						stub_indexes.Add( (int)indexes[i] );
					}
				}
				//Debug.Log( $"3 stub_indexes.Count: {stub_indexes.Count} indexes count: {indexes_count}" );
			}

			if ( updated )
			{
				MeshUpdateFlags flags = MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontResetBoneBounds | MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontNotifyMeshUsers;
				stub_mesh.Clear();
				stub_mesh.SetVertices( stub_vertices, start:0, length: vertices_count, flags: flags );
				if ( vertsGenMode == VertsGeneration.CPU )
				{
					if ( twoTrisPerSegment )
					{
						if ( rel_positions.Count != stub_vertices.Count )
						{
							Debug.LogError( $"rel_positions.Count ({rel_positions.Count}) != stub_vertices.Count ({stub_vertices.Count})" );
						}
						stub_mesh.SetUVs( 0, rel_positions );
						stub_mesh.SetUVs( 1, ab_lengths );
					} else
					{
						stub_mesh.SetUVs( 0, distances );
					}
				}
				stub_mesh.SetIndices( stub_indexes, indicesStart: 0, indicesLength: indexes_count, topology, submesh: 0, calculateBounds: false, baseVertex: 0 );
				stub_mesh.UploadMeshData( markNoLongerReadable: false );
				//Debug.Log( $"updating mesh verts: {vertices_count} indexes: {indexes_count}" );
			}

			command_buffer.DrawMesh( stub_mesh, model_mx, sdfDrawMaterial );

		}
		//#endif
		}

		sdfShadingMaterial.SetFloat( radius_id, radius );
		//command_buffer.Blit(sdf_render_texture.depthBuffer, sdf_render_texture, sdfShadingMaterial );
		var tmp = RenderTexture.GetTemporary( width, height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB );

		command_buffer.Blit( source: sdf_render_texture.depthBuffer, dest: tmp, sdfShadingMaterial );
		command_buffer.CopyTexture( src: tmp, dst: sdf_render_texture );

		Graphics.ExecuteCommandBuffer( command_buffer );

		RenderTexture.ReleaseTemporary( tmp );

		#if false
		Graphics.DrawProceduralIndirect( sdfDrawMaterial,
		                                 bounds,
		                                 topology,
		                                 args_buffer,
		                                 argsOffset:0,
		                                 camera: null,
		                                 properties: null,
		                                 ShadowCastingMode.Off,
		                                 receiveShadows: false,
		                                 layer: 0 );
		#endif
	}
	private Matrix4x4 pos_encode_matrix => Matrix4x4.Scale( new Vector3( 1f / width, 1f / height, 1 ) );
	private Matrix4x4 pos_decode_matrix => Matrix4x4.Scale( new Vector3( width, height, 1 ) );

	[Button]
	private void testCircle()
	{
		segments.Clear();
		polyline.Clear();

		float r = testCircleRadius;
		Vector2 center = new Vector2( width, height ) / 2.0f;
		Vector2 old_p  = center + new Vector2( r, 0 );
		testCircleSides = Mathf.Clamp( testCircleSides, 0, MAX_SEGMENTS_COUNT );

		for ( int i = 0; i != testCircleSides; ++i )
		{
			float rad = ( ( i + 1 ) * Mathf.PI * 2 ) / (float)testCircleSides;
			Vector2 curr_p = center + new Vector2( Mathf.Cos( rad ), Mathf.Sin( rad ) ) * r;
			segments.Add( new Segment( curr_p, old_p ) );
			polyline.Add( old_p );
			old_p = curr_p;
		}
		polyline.Add( old_p );
	}

	[Button]
	private void test()
	{
		//destroy();
		setup();
		if ( mode == Mode.Segments )
		{
			if ( segments.Count == 0 )
			{
				segments.Add( new Segment(){ start = new Vector2(128, 128), end = new Vector2(256,128) } );
			}
			segments_to_triangles( segments, vertsGenMode );
		} else
		{
			if ( polyline.Count == 0 )
			{
				polyline.Add( new Vector2(128, 128) );
				polyline.Add( new Vector2(256, 128) );
			}
			polyline_to_triangles( polyline, vertsGenMode );
		}
		draw_sdf_triangles( vertsGenMode );
		if ( rawImage != null ) rawImage.texture = sdf_render_texture;

		#if false
		Segment[] read_segments  = new Segment[segments.Count];
		Vector2[] positions      = new Vector2[segments.Count * VERTICES_PER_SEGMENT];
		float  [] distances      = new float  [segments.Count * VERTICES_PER_SEGMENT];
		int    [] indices        = new int    [segments.Count * TRIANGLES_PER_SEGMENT * 3];
		Vector4[] debug_vertices = new Vector4[segments.Count * TRIANGLES_PER_SEGMENT * 3];
		segments_buffer     .GetData( read_segments  );
		pos_write_buffer    .GetData( positions      );
		dist_write_buffer   .GetData( distances      );
		indexes_write_buffer.GetData( indices        );
		debug_vertex_buffer .GetData( debug_vertices );
		for ( int i = 0; i != read_segments.Length; ++i )
		{
			Debug.Log( $"segment[{i:00}] = {read_segments[i]}" );
		}
		for ( int i = 0; i != positions.Length; ++i )
		{
			Debug.Log( $"pos[{i:00}] = {positions[i]} dist[{i:00}] = {distances[i]}" );
		}
		for ( int i = 0; i != indices.Length; ++i )
		{
			if ( i % 3 == 0 )
			{
				Debug.Log( $"triangle {i/3:00}" );
			}
			Debug.Log( $"indices[{i:00}] = {indices[i]} pos[{indices[i]:00}] = {positions[indices[i]]}" );
			if ( i % 3 == 2 )
			{
				Debug.Log( $"--  --  --  --  --  --  --  --  --  --" );
			}
		}

		Debug.Log( $"###   ###   ###   ###   ###   ###   ###   ###   ###   ###");

		for ( int i = 0; i != debug_vertices.Length; ++i )
		{
			if ( i % 3 == 0 )
			{
				Debug.Log( $"triangle {i/3:00}" );
			}
			Debug.Log( $"vertices[{i:00}] = {debug_vertices[i]:0.000000} pos[{indices[i]:00}] = {positions[indices[i]]}  dist[{indices[i]:00}] = {distances[indices[i]]}" );
			if ( i % 3 == 2 )
			{
				Debug.Log( $"--  --  --  --  --  --  --  --  --  --" );
			}
		}
		#endif
	}
	#endregion private
}
