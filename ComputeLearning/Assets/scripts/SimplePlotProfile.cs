using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlotProfile : MonoBehaviour
{
	public enum Units
	{
		Secs,
		Millisecs
	}
	public SimplePlot plot;
	public Units      units;

	#region Unity
	private void Update()
	{
		plot._profile = true;
		plot.AddSample( plot._draw_time * ( units == Units.Millisecs ? 1000f : 1f ) );
	}
	#endregion Unity

	#region private
	#endregion private
}
