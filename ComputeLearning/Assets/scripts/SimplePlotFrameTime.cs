using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlotFrameTime : MonoBehaviour
{
	public enum Units
	{
		Secs,
		Millisecs
	}
	public SimplePlot plot;
	public Units      units;

	#region Unity
	private void Update()
	{
		plot.AddSample( Time.deltaTime * ( units == Units.Millisecs ? 1000f : 1f ) );
	}
	#endregion Unity

	#region private
	#endregion private
}
