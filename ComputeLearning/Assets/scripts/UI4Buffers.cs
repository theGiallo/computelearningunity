using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI4Buffers : MonoBehaviour
{
	public RectTransform[] buffers = new RectTransform[4];

	#region Unity
	private void Start()
	{
		for ( int i = 0; i != buffers.Length; ++i )
		{
			renderers[i] = buffers[i].GetComponent<Graphic>();
			if ( renderers[i] == null )
			{
				renderers[i] = buffers[i].GetComponentInChildren<Graphic>();
			}
		}
		update_sizes();
	}
	private void Update()
	{
		if ( Input.GetKeyDown( KeyCode.Space ) )
		{
			int delta = 1;
			if ( Input.GetKey( KeyCode.LeftShift ) || Input.GetKey( KeyCode.RightShift ) )
			{
				delta = -1;
			}
			current_buffer_maximized = ( current_buffer_maximized + delta + 5 ) % 5;

			update_sizes();
		}
	}
	#endregion Unity

	#region private
	private Graphic[] renderers = new Graphic[4];

	const int ALL_SHARED = 4;
	private int current_buffer_maximized = ALL_SHARED;

	private void set_buffer_maximized( int i )
	{
		var rt = buffers[i];
		var r  = renderers[i];
		if ( ! r.enabled ) r.enabled = true;
		rt.anchorMin = Vector2.zero;
		rt.anchorMax = Vector2.one;
	}
	private void set_buffer_shared_space( int i )
	{
		var rt = buffers[i];
		var r  = renderers[i];
		if ( ! r.enabled ) r.enabled = true;

		int y = i / 2;
		var xyi = new Vector2( i - y * 2, y );
		rt.anchorMin = new Vector2( xyi.x * 0.5f, xyi.y * 0.5f );
		rt.anchorMax = rt.anchorMin + new Vector2( 0.5f, 0.5f );
	}
	private void set_buffer_minimized( int i )
	{
		var rt = buffers[i];
		var r  = renderers[i];
		if ( r.enabled ) r.enabled = false;

		rt.anchorMin = Vector2.zero;
		rt.anchorMax = Vector2.zero;
	}
	private void update_sizes()
	{
		if ( current_buffer_maximized == ALL_SHARED )
		{
			for ( int i = 0; i != buffers.Length; ++i )
			{
				set_buffer_shared_space( i );
			}
		} else
		{
			for ( int i = 0; i != buffers.Length; ++i )
			{
				if ( i == current_buffer_maximized )
				{
					set_buffer_maximized( i );
				} else
				{
					set_buffer_minimized( i );
				}
			}
		}
	}
	#endregion private
}
