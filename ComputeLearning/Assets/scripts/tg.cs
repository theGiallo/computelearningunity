﻿#define TG_LOG
#define TG_LOG_WARN
#define TG_LOG_ERR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Runtime.InteropServices;
using System;
using System.Threading.Tasks;

namespace tg
{
	public static partial class tg
	{
		public static void run_task( Action action )
		{
			Task.Run( action ).ConfigureAwait( false );
		}

		#region math
		public enum
		Cartesian_Axis
		{
			X,
			Y,
			Z,
			HORIZONTAL = X,
			VERTICAL   = Y,
		}
		public static
		Vector3
		random_V3( Vector3 min, Vector3 max )
		{
			Vector3 ret = new Vector3();
			for ( int i = 0; i != 3; ++i )
			{
				ret[i] = UnityEngine.Random.Range( min[i], max[i] );
			}
			return ret;
		}
		public unsafe struct
		V2
		{
			public fixed float arr[2];
			public float x => arr[0];
			public float y => arr[1];
			public V2 xy { get { return new V2( x, y ); } set { arr[0] = value.arr[0]; arr[1] = value.arr[1]; } }
			public V2 yx { get { return new V2( y, x ); } set { arr[1] = value.arr[0]; arr[0] = value.arr[0]; } }
			public V2 xx => new V2( x, x );
			public V2 yy => new V2( y, y );
			public float this[int i] => arr[i];

			public static V2 operator +( V2 a, float b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] + b;
				ret.arr[1] = a.arr[1] + b;
				return ret;
			}
			public static V2 operator -( V2 a, float b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] - b;
				ret.arr[1] = a.arr[1] - b;
				return ret;
			}
			public static V2 operator *( V2 a, float b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] * b;
				ret.arr[1] = a.arr[1] * b;
				return ret;
			}
			public static V2 operator /( V2 a, float b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] / b;
				ret.arr[1] = a.arr[1] / b;
				return ret;
			}
			public static V2 operator +( float a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a + b.arr[0];
				ret.arr[1] = a + b.arr[1];
				return ret;
			}
			public static V2 operator -( float a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a - b.arr[0];
				ret.arr[1] = a - b.arr[1];
				return ret;
			}
			public static V2 operator *( float a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a * b.arr[0];
				ret.arr[1] = a * b.arr[1];
				return ret;
			}
			public static V2 operator /( float a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a / b.arr[0];
				ret.arr[1] = a / b.arr[1];
				return ret;
			}
			public static V2 operator +( V2 a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] + b.arr[0];
				ret.arr[1] = a.arr[1] + b.arr[1];
				return ret;
			}
			public static V2 operator -( V2 a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] - b.arr[0];
				ret.arr[1] = a.arr[1] - b.arr[1];
				return ret;
			}
			public static V2 operator *( V2 a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] * b.arr[0];
				ret.arr[1] = a.arr[1] * b.arr[1];
				return ret;
			}
			public static V2 operator /( V2 a, V2 b )
			{
				V2 ret = new V2();
				ret.arr[0] = a.arr[0] / b.arr[0];
				ret.arr[1] = a.arr[1] / b.arr[1];
				return ret;
			}

			public static implicit operator V2( Vector2 v )
			{
				V2 ret = new V2( v.x, v.y );
				return ret;
			}
			public static implicit operator Vector2( V2 v )
			{
				Vector2 ret = new Vector2( v.x, v.y );
				return ret;
			}

			public V2( float v )
			{
				arr[0] = arr[1] = v;
			}
			public V2( float x, float y )
			{
				arr[0] = x;
				arr[1] = y;
			}
			public V2( V2 v )
			{
				arr[0] = v.arr[0];
				arr[1] = v.arr[1];
			}
		}

		public static
		V2
		smoothstep( V2 t01 )
		{
			V2 ret = t01 * t01 * ( 3f - 2f * t01 );
			return ret;
		}
		public static
		float
		smoothstep( float t01 )
		{
			float ret = t01 * t01 * ( 3f - 2f * t01 );
			return ret;
		}
		public static
		V2
		smootherstep( V2 t01 )
		{
			V2 ret = t01 * t01 * t01 * ( t01 * ( t01 * 6f - 15f ) + 10f );
			return ret;
		}
		public static
		float
		smootherstep( float t01 )
		{
			float ret = t01 * t01 * t01 * ( t01 * ( t01 * 6f - 15f ) + 10f );
			return ret;
		}
		public static
		float
		fast_bell_random( float center, float radius, int iterations = 4 )
		{
			float ret = 0;
			float min = center - radius * 0.5f;
			float max = center + radius * 0.5f;
			for ( int i = 0; i != iterations; ++i )
			{
				ret += UnityEngine.Random.Range( min, max );
			}
			ret /= (float)iterations;
			return ret;
		}
		public static
		float
		random_sign()
		{
			float ret = UnityEngine.Random.value > 0.5f ? 1f : -1f;
			return ret;
		}
		#endregion math

		#region logging
		public static
		void
		log( string msg, GameObject go = null )
		{
			#if TG_LOG
			Debug.Log( msg, go );
			#endif
		}
		public static
		void
		log_warn( string msg, GameObject go = null )
		{
			#if TG_LOG_WARN
			Debug.LogWarning( msg, go );
			#endif
		}
		public static
		void
		log_err( string msg, GameObject go = null )
		{
			#if TG_LOG_ERR
			Debug.LogError( msg, go );
			#endif
		}
		#endregion logging

		public static
		void
		destroy_all_children( this MonoBehaviour m )
		{
			m.gameObject.destroy_all_children();
		}
		public static
		void
		destroy_all_children( this GameObject go )
		{
			if ( Application.isPlaying )
			{
				for ( int i = go.transform.childCount - 1; i != -1; --i )
				{
					UnityEngine.Object.Destroy( go.transform.GetChild( i ).gameObject );
				}
			} else
			{
				for ( int i = go.transform.childCount - 1; i != -1; --i )
				{
					UnityEngine.Object.DestroyImmediate( go.transform.GetChild( i ).gameObject );
				}
			}
		}

		public enum
		Chained
		{
			INTERRUPT,
			CONTINUE,
		}
		public class
		Event_With_Priority_Interruptable<T> : Event_With_Priority_Interruptable<T>.ISubscribable
		{
			public interface
			ISubscribable
			{
				int
				Subscribe( ushort priority, Func<T,Chained> callback );
				bool
				Unsubscribe( int priority_i );
				void
				Unsubscribe( Func<T,Chained> callback );
			}

			public
			int
			Subscribe( ushort priority, Func<T,Chained> callback )
			{
				int priority_i = priority << 16;
				while ( callbacks.ContainsKey( priority_i ) )
				{
					++priority_i;
				}
				callbacks.Add( priority_i, callback );
				return priority_i;
			}
			public
			bool
			Unsubscribe( int priority_i )
			{
				return callbacks.Remove( priority_i );
			}
			public
			void
			Unsubscribe( Func<T,Chained> callback )
			{
				int i = callbacks.IndexOfValue( callback );
				if ( i >= 0 )
				{
					callbacks.RemoveAt( i );
				}
			}

			public int count => callbacks.Count;
			public
			void
			Invoke( T v )
			{
				var values = callbacks.Values;
				foreach ( var c in values )
				{
					if ( c.Invoke( v ) == Chained.INTERRUPT )
					{
						break;
					}
				}
			}
			public
			IEnumerator
			InvokeEn( T v )
			{
				var values = callbacks.Values;
				foreach ( var c in values )
				{
					if ( c.Invoke( v ) == Chained.INTERRUPT )
					{
						yield break;
					}
					yield return null;
				}
			}

			#region private
			private SortedList<int,Func<T,Chained>> callbacks = new SortedList<int, Func<T,Chained>>();
			#endregion private
		}
		public class
		Event_With_Priority<T> : Event_With_Priority<T>.ISubscribable
		{
			public interface
			ISubscribable
			{
				int
				Subscribe( ushort priority, Action<T> callback );
				bool
				Unsubscribe( int priority_i );
				void
				Unsubscribe( Action<T> callback );
			}

			public
			int
			Subscribe( ushort priority, Action<T> callback )
			{
				int priority_i = priority << 16;
				while ( callbacks.ContainsKey( priority_i ) )
				{
					++priority_i;
				}
				callbacks.Add( priority_i, callback );
				return priority_i;
			}

			public
			bool
			Unsubscribe( int priority_i )
			{
				return callbacks.Remove( priority_i );
			}

			public
			void
			Unsubscribe( Action<T> callback )
			{
				int i = callbacks.IndexOfValue( callback );
				if ( i >= 0 )
				{
					callbacks.RemoveAt( i );
				}
			}

			public int count => callbacks.Count;
			public
			void
			Invoke( T v )
			{
				var values = callbacks.Values;
				foreach ( var c in values )
				{
					c.Invoke( v );
				}
			}
			public
			IEnumerator
			InvokeEn( T v )
			{
				var values = callbacks.Values;
				foreach ( var c in values )
				{
					c.Invoke( v );
					yield return null;
				}
			}



			#region private
			private SortedList<int,Action<T>> callbacks = new SortedList<int, Action<T>>();
			#endregion private
		}

		//[StructLayout(LayoutKind.Explicit)]
		public class OptErr<T,E>
		{
			public enum Type
			{
				ERROR,
				VALUE
			}
			public bool is_error => type == Type.ERROR;
			public bool is_value => type == Type.VALUE;
			public Type type { get => _type; private set{ _type = value; } }
			/*[FieldOffset(0)]*/ private Type _type;
			/*[FieldOffset(8)]*/ private T    value;
			/*[FieldOffset(8)]*/ private E    error;

			public static implicit operator T( OptErr<T,E> opt_err )
			{
				if ( opt_err.type != Type.VALUE )
				{
					ILLEGAL_PATH();
					return default;
				}
				return opt_err.value;
			}
			public static implicit operator E( OptErr<T,E> opt_err )
			{
				if ( opt_err.type != Type.ERROR )
				{
					ILLEGAL_PATH();
					return default;
				}
				return opt_err.error;
			}
			public static implicit operator OptErr<T,E>( T v )
			{
				OptErr<T,E> ret = new OptErr<T,E>();
				ret.value = v;
				ret.type = Type.VALUE;
				return ret;
			}
			public static implicit operator OptErr<T,E>( E e )
			{
				OptErr<T,E> ret = new OptErr<T,E>();
				ret.error = e;
				ret.type = Type.ERROR;
				return ret;
			}
		}

		public class Opt<T>
		{
			public bool has_value { get; private set; }
			private T _value;
			public T value { get { debug_assert( has_value ); return _value; } private set { _value = value; } }

			public static Opt<T> NO_VALUE => new Opt<T>();

			public Opt()
			{
				has_value = false;
				value     = default;
			}

			public static implicit operator T( Opt<T> opt )
			{
				if ( ! opt.has_value )
				{
					ILLEGAL_PATH();
					return default;
				}
				return opt.value;
			}
			public static implicit operator Opt<T>( T v )
			{
				Opt<T> ret = new Opt<T>();
				ret.value = v;
				ret.has_value = true;
				return ret;
			}

			public override string ToString()
			{
				string ret = has_value ? _value.ToString() : "NO_VALUE";
				return ret;
			}
		}

		unsafe public static
		bool
		has_flags<T>( T flags, T flags_to_check ) where T : unmanaged, Enum
		{
			bool ret;

			if ( sizeof( T ) == sizeof( byte ) )
			{
				byte uflags           = *(byte*)&flags,
				     uflags_to_check  = *(byte*)&flags_to_check;
				ret = ( uflags & uflags_to_check ) == uflags_to_check;
			} else
			if ( sizeof( T ) == sizeof( ushort ) )
			{
				ushort uflags           = *(ushort*)&flags,
				       uflags_to_check  = *(ushort*)&flags_to_check;
				ret = ( uflags & uflags_to_check ) == uflags_to_check;
			} else
			if ( sizeof( T ) == sizeof( uint ) )
			{
				uint uflags           = *(uint*)&flags,
				     uflags_to_check  = *(uint*)&flags_to_check;
				ret = ( uflags & uflags_to_check ) == uflags_to_check;
			} else
			if ( sizeof( T ) == sizeof( ulong ) )
			{
				ulong uflags           = *(ulong*)&flags,
				      uflags_to_check  = *(ulong*)&flags_to_check;
				ret = ( uflags & uflags_to_check ) == uflags_to_check;
			} else
			{
				ret = false;
			}
			return ret;
		}

		public static
		T[]
		ToArray<T>( this HashSet<T> h )
		{
			T[] ret = new T[h.Count];
			int i = 0;
			foreach ( T e in h )
			{
				ret[i++] = e;
			}
			return ret;
		}
		public static Opt<T> First<T>( this HashSet<T> h )
		{
			Opt<T> ret = Opt<T>.NO_VALUE;
			foreach ( T e in h )
			{
				ret = e;
				break;
			}
			return ret;
		}
		public static int Add<T>( this HashSet<T> h, HashSet<T> n )
		{
			int ret = 0;
			foreach ( T e in n )
			{
				if ( h.Add( e ) )
				{
					++ret;
				}
			}
			return ret;
		}

		public static void Add<T>( this List<T> l, List<T> added )
		{
			for ( int i = 0; i != added.Count; ++i )
			{
				l.Add( added[i] );
			}
		}
		public static void CopyFrom<T>( this List<T> l, List<T> source )
		{
			l.Clear();
			l.Add( source );
		}
		public static void CopyInto<T>( this List<T> l, List<T> dest )
		{
			dest.CopyFrom( l );
		}
		public static List<T> Clone<T>( this List<T> l )
		{
			List<T> ret = new List<T>();
			ret.Add( l );
			return ret;
		}


		public static
		T[]
		initialized_array<T>( int length ) where T : new()
		{
			T[] ret = new T[length];
			for ( int i = 0; i != length; ++i )
			{
				ret[i] = new T();
			}
			return ret;
		}


		public static
		void
		debug_assert( bool cond, string msg = "" )
		{
			#if DEBUG
			if ( ! cond )
			{
				log_err( $"ASSERT FAILED{(msg!=""?" | " + msg:"")}" );
				if ( ! System.Diagnostics.Debugger.IsAttached )
				{
					try
					{
						System.Diagnostics.Debugger.Launch();
					} catch ( NotImplementedException )
					{
					}
				}
				System.Diagnostics.Debugger.Break();
			}
			#endif
		}
		public static
		void
		ILLEGAL_PATH( string msg = "" )
		{
			log_err( $"ILLEGAL PATH | {msg}" );
			if ( ! System.Diagnostics.Debugger.IsAttached )
			{
				try {
					System.Diagnostics.Debugger.Launch();
				} catch ( NotImplementedException )
				{}
			}
			System.Diagnostics.Debugger.Break();
		}
	}
}