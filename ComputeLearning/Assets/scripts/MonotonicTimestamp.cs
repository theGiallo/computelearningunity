﻿#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
#define NATIVE_TIMER
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using static tg.tg;
using System;
using System.Runtime.InteropServices;

namespace tg
{
	public struct MonotonicTimestamp
	{
		private static double tickFrequency;

#if NATIVE_TIMER
		private long timestamp;
#else
		private DateTime datetime;
#endif

		static MonotonicTimestamp()
		{
			long frequency;
			bool succeeded = NativeMethods.QueryPerformanceFrequency( out frequency );
			if ( !succeeded )
			{
				throw new System.PlatformNotSupportedException( "Requires Windows XP or later" );
			}

			tickFrequency = (double)System.TimeSpan.TicksPerSecond / frequency;
		}

#if NATIVE_TIMER
		private MonotonicTimestamp(long timestamp)
		{
			this.timestamp = timestamp;
		}
#else
		private MonotonicTimestamp( DateTime datetime )
		{
			this.datetime = datetime;
		}
#endif

		static private DateTime origin = new DateTime( 1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc );
		public static MonotonicTimestamp Now()
		{
#if NATIVE_TIMER
			long value;
			NativeMethods.QueryPerformanceCounter(out value);
			return new MonotonicTimestamp(value);
#else
			DateTime dt = DateTime.Now;
			//TimeSpan ts = dt - origin;
			//return new MonotonicTimestamp( (long)ts.TotalMilliseconds );
			return new MonotonicTimestamp( dt );
#endif
		}

		public static System.TimeSpan operator -( MonotonicTimestamp to, MonotonicTimestamp from )
		{
#if NATIVE_TIMER
			if (to.timestamp == 0)
				throw new System.ArgumentException("Must be created using MonotonicTimestamp.Now(), not default(MonotonicTimestamp)", nameof(to));
			if (from.timestamp == 0)
				throw new System.ArgumentException("Must be created using MonotonicTimestamp.Now(), not default(MonotonicTimestamp)", nameof(from));

			long ticks = unchecked((long)((to.timestamp - from.timestamp) * tickFrequency));
			return new System.TimeSpan(ticks);
#else
			return to.datetime - from.datetime;
#endif
		}

		private static class NativeMethods
		{
#if NATIVE_TIMER
			[System.Runtime.InteropServices.DllImport("kernel32.dll")]
			public static extern bool QueryPerformanceCounter(out long value);

			[System.Runtime.InteropServices.DllImport("kernel32.dll")]
			public static extern bool QueryPerformanceFrequency(out long value);
#else
			public static bool QueryPerformanceCounter( out long value ) { value = 1; return true; }
			public static bool QueryPerformanceFrequency( out long value ) { value = 1; return true; }
#endif
		}
	}
}
