using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
//using Button = NaughtyAttributes.Button;
using RawImage = UnityEngine.UI.RawImage;

namespace tg
{
	public class GrayScott : MonoBehaviour
	{
		public enum
		UpdateMode
		{
			AUTO,
			MANUAL,
		}

		public enum
		ViewMode
		{
			INIT,
			CURRENT,
		}
		public enum
		InitType
		{
			BW,
			RGB,
			RG_EXCLUSIVE,
			RG_EXCLUSIVE_OR_ZERO,
			CENTRAL_CIRCLE,
			CENTRAL_CIRCLE_GRAD,
			GRADIENT_XY
		}
		[Serializable]
		public
		class
		Parameters
		{
			public string name;
			public float dt = 1f;
			public float f  = 0.055f;
			public float k  = 0.062f;
			public float DA = 1.0f;
			public float DB = 0.5f;
		}
		public InitType initType;
		public ViewMode viewMode;
		public UpdateMode updateMode;
		public RenderTextureFormat renderTextureFormat = RenderTextureFormat.RGFloat;
		public Parameters[] parametersSets;
		[Dropdown("parametersSetsIndexes")]
		public int parametersIndex;
		#if UNITY_EDITOR
		private int[] parametersSetsIndexes()
		{
			int[] ret = new int[parametersSets.Length];
			for ( int i = 0; i != parametersSets.Length; ++i )
			{
				ret[i] = i;
			}
			return ret;
		}
		#endif
		public ref Parameters parameters => ref parametersSets[parametersIndex];
		public float circleRadius = 128;
		public float pZero = 0.8f;
		public float pRed  = 0.7f;

		public enum ComputingMode
		{
			COMPUTE,
			FRAGMENT,
			FRAGMENT_TEXTURE_WRITE
		}
		[Space]
		public ComputingMode computingMode;
		public ComputeShader computeShader;
		public Shader        shader;
		public Shader        shaderTextureWrite;

		public bool hasFKRange;
		[ShowIf("hasFKRange")]
		public Vector2 minFK;
		[ShowIf("hasFKRange")]
		public Vector2 maxFK;

		[Space]
		public int w = 1024, h = 1024;
		public RawImage[] rawImagesInit;
		public RawImage[] rawImages;
		public int initRandN = 1;

		[Space]
		public int framesPeriod = 1;
		public int updatesPerFrame = 1;

		[Space]
		//[ShowIf("saveSequenceEnabled")]
		public Material saveMaterial;

		[Space]
		public int  jpgQuality = 90;
		public bool saveEachRun;
		public bool saveEachFrame;
		public bool saveSequenceEnabled;
		[ShowIf("saveSequenceEnabled")]
		public float differenceCheckPeriodSecs = 30f;
		[ShowIf("saveSequenceEnabled"), Range(0,1)]
		public float saveMaxDifferenceThreshold = 0.02f;
		[ShowIf("saveSequenceEnabled")]
		public float maxTotalDurationSecs = 60f * 5f;
		[ShowIf("saveSequenceEnabled"), Range(0,1)]
		public float fIncrement = 0;
		[ShowIf("saveSequenceEnabled"), Range(0,1)]
		public float kIncrement = 0.001f;

		#region Unity
		private void Start()
		{
			init();

			differenceTimerStart = Time.realtimeSinceStartup;
		}
		private void Update()
		{
			if ( Input.GetKeyDown( KeyCode.S ) )
			{
				if ( Input.GetKey( KeyCode.LeftControl ) || Input.GetKey( KeyCode.RightControl ) )
				{
					saveToPNG();
				}
			}

			if ( Input.GetKeyDown( KeyCode.F5 ) )
			{
				if ( Input.GetKey( KeyCode.LeftShift ) || Input.GetKey( KeyCode.RightShift ) )
				{
					regenerate();
				}
				restart();
			}

			if ( Input.GetKeyDown( KeyCode.F7 ) )
			{
				saveEachRun = false;
			} else
			if ( Input.GetKeyDown( KeyCode.F8 ) )
			{
				saveEachRun = true;
				saveEachFrame = false;
			}

			if ( Input.GetKeyDown( KeyCode.F10 ) )
			{
				saveEachFrame = false;
			} else
			if ( Input.GetKeyDown( KeyCode.F11 ) )
			{
				saveEachFrame = true;
				saveEachRun = false;
			}

			if ( saveEachFrame )
			{
				saveToJPG();
			}

			if ( updateMode == UpdateMode.AUTO )
			{
				if ( ( Time.frameCount % framesPeriod ) == 0 )
				{
					for ( int i = 0; i != ( saveEachRun ? 1 : updatesPerFrame ); ++i )
					{
						run();
					}
				}

				if ( saveSequenceEnabled && ( difference_timer_ended() || simDuration > maxTotalDurationSecs ) )
				{
					if_different_save_and_advance();
				}
			}
		}
		#endregion Unity

		#region private
		private struct
		Data
		{
			public float r, g, b;
		}
		//private ComputeBuffer compute_buffer;
		private Data[] data;

		private RenderTexture[] render_textures;
		private RenderTexture save_rt;
		private RenderTexture render_texture_read => render_textures[read_index];
		private RenderTexture render_texture_write => render_textures[write_index];
		private RenderTexture render_texture_write_temp => render_textures[2];
		int read_index = 0, write_index = 1;
		private Texture2D     init_texture;

		private Material gray_scott_shader_mat;
		private Material gray_scott_shader_tex_w_mat;

		private int kernel_id;
		private int read_texture_id;
		private int write_texture_id;
		private int main_tex_id;
		private int write_tex_id;

		private int parameter_id_dt;
		private int parameter_id_f ;
		private int parameter_id_k ;
		private int parameter_id_fk_min_max;
		private int parameter_id_DA;
		private int parameter_id_DB;

		private int items_length, item_size;

		private int iteration_counter;
		private uint used_seed;

		private Squirrel3 rand;



		private Texture2D tex;
		private float differenceTimerStart;

		private float simDuration => Time.realtimeSinceStartup - sim_start_time;
		private float sim_start_time;

		private
		void
		init()
		{
			items_length = w * h;
			item_size = 4 * 3;

			rand = new Squirrel3();
			Debug.Log( $"seed {rand.seed}" );
			Debug.Log( $"{w}x{h}: {items_length} items" );

			main_tex_id      = Shader.PropertyToID( "_MainTex" );
			write_tex_id     = Shader.PropertyToID( "_WriteTex" );
			write_texture_id = Shader.PropertyToID( "write_texture" );
			read_texture_id  = Shader.PropertyToID( "read_texture" );
			parameter_id_dt  = Shader.PropertyToID( "dt" );
			parameter_id_f   = Shader.PropertyToID( "f"  );
			parameter_id_k   = Shader.PropertyToID( "k"  );
			parameter_id_fk_min_max = Shader.PropertyToID( "fk_min_max"  );
			parameter_id_DA  = Shader.PropertyToID( "DA" );
			parameter_id_DB  = Shader.PropertyToID( "DB" );

			gray_scott_shader_mat       = new Material( shader );
			gray_scott_shader_tex_w_mat = new Material( shaderTextureWrite );

			regenerate();

			restart();

			int bytes_count = item_size * items_length;
			Debug.Log( $"{bytes_count}B" );

			int kernel_id = computeShader.FindKernel( "CSMain" );

			#if false
			compute_buffer = new ComputeBuffer( items_length, item_size );
			compute_buffer.SetData( data );
			computeShader.SetBuffer( kernel_id, "dataBuffer", compute_buffer );
			#endif
		}
		[NaughtyAttributes.Button]
		private
		void
		restart()
		{
			sim_start_time = Time.realtimeSinceStartup;
			iteration_counter = 0;

			Graphics.Blit( source: init_texture, dest: render_texture_read );

			update_view();
		}
		[NaughtyAttributes.Button]
		private
		void
		run()
		{
			switch ( computingMode )
			{
				case ComputingMode.COMPUTE:
				{
					computeShader.SetTexture( kernel_id, read_texture_id , render_texture_read  );
					computeShader.SetTexture( kernel_id, write_texture_id, render_texture_write );

					computeShader.SetFloat( parameter_id_dt, parameters.dt );
					if ( hasFKRange )
					{
						computeShader.SetVector( parameter_id_fk_min_max, new Vector4( minFK.x, minFK.y, maxFK.x, maxFK.y ) );
					} else
					{
						computeShader.SetFloat( parameter_id_f , parameters.f  );
						computeShader.SetFloat( parameter_id_k , parameters.k  );
					}
					computeShader.SetFloat( parameter_id_DA, parameters.DA );
					computeShader.SetFloat( parameter_id_DB, parameters.DB );

					computeShader.GetKernelThreadGroupSizes( kernel_id,
					                                         out uint thread_group_size_x,
					                                         out uint thread_group_size_y,
					                                         out uint thread_group_size_z );
					int threadGroupsX = ( w + (int)thread_group_size_x - 1 ) / (int)thread_group_size_x;
					int threadGroupsY = ( h + (int)thread_group_size_y - 1 ) / (int)thread_group_size_y;
					int threadGroupsZ = ( 1 + (int)thread_group_size_z - 1 ) / (int)thread_group_size_z;

					computeShader.Dispatch( kernel_id, threadGroupsX: threadGroupsX, threadGroupsY: threadGroupsY, threadGroupsZ: threadGroupsZ );
					break;
				}
				case ComputingMode.FRAGMENT:
				{
					gray_scott_shader_mat.SetTexture( main_tex_id , render_texture_read );

					gray_scott_shader_mat.SetFloat( parameter_id_dt, parameters.dt );
					if ( hasFKRange )
					{
						gray_scott_shader_mat.SetVector( parameter_id_fk_min_max, new Vector4( minFK.x, minFK.y, maxFK.x, maxFK.y ) );
					} else
					{
						gray_scott_shader_mat.SetFloat( parameter_id_f , parameters.f  );
						gray_scott_shader_mat.SetFloat( parameter_id_k , parameters.k  );
					}
					gray_scott_shader_mat.SetFloat( parameter_id_DA, parameters.DA );
					gray_scott_shader_mat.SetFloat( parameter_id_DB, parameters.DB );

					Graphics.Blit( render_texture_read, render_texture_write, gray_scott_shader_mat );
					break;
				}
				case ComputingMode.FRAGMENT_TEXTURE_WRITE:
				{
					//Graphics.SetRandomWriteTarget( 3, render_texture_write );

					gray_scott_shader_tex_w_mat.SetTexture( main_tex_id , render_texture_read );
					gray_scott_shader_tex_w_mat.SetTexture( write_tex_id, render_texture_write );

					gray_scott_shader_tex_w_mat.SetFloat( parameter_id_dt, parameters.dt );
					if ( hasFKRange )
					{
						gray_scott_shader_tex_w_mat.SetVector( parameter_id_fk_min_max, new Vector4( minFK.x, minFK.y, maxFK.x, maxFK.y ) );
					} else
					{
						gray_scott_shader_tex_w_mat.SetFloat( parameter_id_f , parameters.f  );
						gray_scott_shader_tex_w_mat.SetFloat( parameter_id_k , parameters.k  );
					}
					gray_scott_shader_tex_w_mat.SetFloat( parameter_id_DA, parameters.DA );
					gray_scott_shader_tex_w_mat.SetFloat( parameter_id_DB, parameters.DB );

					Graphics.SetRandomWriteTarget( 3, render_texture_write );
					RenderTexture tempRT = RenderTexture.GetTemporary( render_texture_read.width,
					                                                   render_texture_read.height,
					                                                   render_texture_read.depth,
					                                                   render_texture_read.format,
					                                                   RenderTextureReadWrite.Linear );

					Graphics.Blit( render_texture_read, tempRT, gray_scott_shader_tex_w_mat );

					RenderTexture.ReleaseTemporary( tempRT );
					Graphics.ClearRandomWriteTargets();

					break;
				}
			}

			++iteration_counter;

			swap();
			update_view();

			if ( saveEachRun )
			{
				saveToJPG();
			}
		}
		private
		void
		swap()
		{
			    int tmp = write_index;
			write_index =  read_index;
			 read_index = tmp;
		}

		private
		void
		update_view()
		{
			for ( int i = 0; i != rawImagesInit.Length; ++i )
			{
				rawImagesInit[i].texture = init_texture;
			}
			for ( int i = 0; i != rawImages.Length; ++i )
			{
				switch ( viewMode )
				{
					case ViewMode.INIT:
						rawImages[i].texture = init_texture;
						break;
					case ViewMode.CURRENT:
						rawImages[i].texture = render_texture_read;
						break;
				}
			}
		}

		[Button]
		private
		void
		regenerate()
		{
			items_length = w * h;

			generate_data();

			if ( render_textures == null )
			{
				render_textures = new RenderTexture[3];
			}

			for ( int i = 0; i != render_textures.Length; ++i )
			{
				ref var render_texture = ref render_textures[i];

				if ( render_texture != null
				  && ( w != render_texture.width || h != render_texture.height || render_texture.format != renderTextureFormat )
				  )
				{
					render_texture.Release();
					render_texture = null;
				}

				if ( render_texture == null )
				{
					//render_texture = new RenderTexture( w, h, 0, renderTextureFormat, 0 );
					render_texture = new RenderTexture( w, h, depth:0, renderTextureFormat, RenderTextureReadWrite.Linear );
					render_texture.useMipMap = false;
					render_texture.autoGenerateMips = false;
					render_texture.wrapMode = TextureWrapMode.Repeat;
					render_texture.enableRandomWrite = true;
					bool created = render_texture.Create();
					if ( ! created )
					{
						Debug.LogError( $"failed to create render texture" );
						return;
					}
				}
			}


			if ( init_texture != null && ( w != init_texture.width || h != init_texture.height ) )
			{
				init_texture = null;
			}

			if ( init_texture == null )
			{
				init_texture = new Texture2D( w, h, TextureFormat.RGB24, false );
			}

			{
				Color[] colors = new Color[ data.Length ];
				for ( int i = 0; i != data.Length; ++i )
				{
					colors[i] = new Color( data[i].r, data[i].g, data[i].b, 0 );
				}
				init_texture.SetPixels( colors );
				init_texture.Apply();
			}
		}
		private
		void
		generate_data()
		{
			used_seed = rand.seed;
			if ( data != null && data.Length != items_length )
			{
				data = null;
			}
			if ( data == null )
			{
				data = new Data[items_length];
			}
			switch ( initType )
			{
				case InitType.BW:
					for ( int i = 0; i != data.Length; ++i )
					{
						data[i].r =
						data[i].g =
						data[i].b = rand.NextFloat01NAVG( initRandN );
					}
					break;
				case InitType.RGB:
					for ( int i = 0; i != data.Length; ++i )
					{
						data[i].r = rand.NextFloat01NAVG( initRandN );
						data[i].g = rand.NextFloat01NAVG( initRandN );
						data[i].b = rand.NextFloat01NAVG( initRandN );
					}
					break;
				case InitType.RG_EXCLUSIVE:
					for ( int i = 0; i != data.Length; ++i )
					{
						float p = rand.NextFloat01NAVG( initRandN );
						bool b = p < pRed;
						data[i].r = b ? 1 : 0;
						data[i].g = b ? 0 : 1;
						data[i].b = 0;
					}
					break;
				case InitType.RG_EXCLUSIVE_OR_ZERO:
					for ( int i = 0; i != data.Length; ++i )
					{
						float pZ = rand.NextFloat01NAVG( initRandN );
						float pR = rand.NextFloat01NAVG( initRandN );
						bool z = pZ < pZero;
						bool b = pR < pRed;
						if ( z )
						{
							data[i].r = 0;
							data[i].g = 0;
							data[i].b = 0;
						} else
						{
							data[i].r = b ? 1 : 0;
							data[i].g = b ? 0 : 1;
							data[i].b = 0;
						}
					}
					break;
				case InitType.CENTRAL_CIRCLE:
				{
					Vector2 center = new Vector2( w, h ) / 2f;
					for ( int i = 0; i != data.Length; ++i )
					{
						int y = i / w;
						int x = i - y * w;
						Vector2 p = new Vector2( x, y );
						bool inside = ( p - center ).magnitude < circleRadius;
						if ( inside )
						{
							data[i].r = 1;
							data[i].g = 1;
							data[i].b = 0;
						} else
						{
							data[i].r = 1;
							data[i].g = 0;
							data[i].b = 0;
						}
					}
				}
				break;
				case InitType.CENTRAL_CIRCLE_GRAD:
				{
					Vector2 center = new Vector2( w, h ) / 2f;
					for ( int i = 0; i != data.Length; ++i )
					{
						int y = i / w;
						int x = i - y * w;
						Vector2 p = new Vector2( x, y );
						float d = ( p - center ).magnitude;
						float norm = d / circleRadius;
						data[i].r = 1;
						data[i].g = Mathf.Abs( norm % 1 );
						data[i].b = 0;
					}
				}
				break;
				case InitType.GRADIENT_XY:
				{
					Vector2 center = new Vector2( w, h ) / 2f;
					for ( int i = 0; i != data.Length; ++i )
					{
						int y = i / w;
						int x = i - y * w;
						Vector2 rg = new Vector2( x / (float)w, y / (float)h );
						data[i].r = rg.x;
						data[i].g = rg.y;
						data[i].b = 0;
					}
				}
				break;
			}
		}

		private
		void
		to_tex( RenderTexture rt )
		{
			if ( tex == null || tex.width != w || tex.height != h )
			{
				tex = new Texture2D( w, h, TextureFormat.ARGB32, false, linear: true );
			}

			#if true
			Rect rectReadPicture = new Rect( 0, 0, w, h );

			RenderTexture old = RenderTexture.active;

			RenderTexture tempRT = RenderTexture.GetTemporary( rt.width, rt.height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear );
			Graphics.Blit( rt, tempRT );
			RenderTexture.active = tempRT;

			tex.ReadPixels( rectReadPicture, 0, 0, false );

			RenderTexture.active = old;

			//tempRT.Release();
			RenderTexture.ReleaseTemporary( tempRT );
			#else
			Graphics.CopyTexture( src:rt, dst:tex );
			#endif
		}
		private
		Color32[]
		retrieve_data( RenderTexture rt )
		{
			to_tex( rt );
			return get_tex_pixels();
		}
		private
		Color32[]
		get_tex_pixels()
		{
			Color32[] ret = tex.GetPixels32();
			return ret;
		}
		private
		bool
		stepsWereDifferent( float threshold_percentage_max = 0.02f )
		{
			bool ret = true;
			Color32[] pixels_write = retrieve_data( render_texture_write );
			Color32[] pixels_read  = retrieve_data( render_texture_read  );
			float r = 0, g = 0, b = 0;
			float max_r = 0, max_g = 0, max_b = 0;
			for ( int i = 0; i != pixels_read.Length; ++i )
			{
				Color32 cr = pixels_read[i];
				Color32 cw = pixels_write[i];
				float this_r = Mathf.Abs( cw.r - cr.r ) / 255f;
				float this_g = Mathf.Abs( cw.g - cr.g ) / 255f;
				float this_b = Mathf.Abs( cw.b - cr.b ) / 255f;
				max_r = Mathf.Max( max_r, this_r );
				max_g = Mathf.Max( max_g, this_g );
				max_b = Mathf.Max( max_b, this_b );
				r += this_r;
				g += this_g;
				b += this_b;
			}
			Debug.Log( $"max r: {max_r} g: {max_g} b: {max_b}" );
			ret = max_r < threshold_percentage_max
			   && max_g < threshold_percentage_max
			   && max_b < threshold_percentage_max;
			return ret;
		}

		[Button]
		private
		void
		saveToPNG() => save( SaveFormat.PNG );

		[Button]
		private
		void
		saveToJPG() => save( SaveFormat.JPG );

		public enum SaveFormat
		{
			PNG,
			JPG
		}
		private void save( SaveFormat save_format )
		{
			if ( save_rt == null || save_rt.width != w || save_rt.height != h )
			{
				//save_rt = new RenderTexture( render_texture_read );
				save_rt = new RenderTexture( w, h, 0, RenderTextureFormat.ARGB32, 0 );
			}

			if ( saveMaterial == null )
			{
				Graphics.Blit( source: render_texture_read, dest: save_rt );
			} else
			{
				Graphics.Blit( source: render_texture_read, dest: save_rt, saveMaterial );
			}
			to_tex( save_rt );

			byte[] blob = null;
			switch ( save_format )
			{
				case SaveFormat.PNG: blob = tex.EncodeToPNG(); break;
				case SaveFormat.JPG: blob = tex.EncodeToJPG( jpgQuality ); break;
			}

			string extension = "";
			switch ( save_format )
			{
				case SaveFormat.PNG: extension = ".png"; break;
				case SaveFormat.JPG: extension = ".jpg"; break;
			}

			string it_string = $"_seed{used_seed}_it{iteration_counter:D06}";

			string folder_name;
			string file_name;
			if ( saveSequenceEnabled )
			{
				folder_name = $"reaction_diffusion_steps_f{fIncrement:0.000000}_k{kIncrement:0.000000}";
				file_name   = $"f{parameters.f:0.000000}_k{parameters.k:0.000000}{it_string}{extension}";
			} else
			{
				folder_name = $"gray-scott";
				if ( hasFKRange )
				{
					file_name = $"gray-scott_FKRange_f{minFK.x}-{maxFK.x}_k{minFK.y}-{maxFK.y}{it_string}{extension}";
				} else
				{
					file_name = $"gray-scott_f{parameters.f}_k{parameters.k}{it_string}{extension}";
				}
			}
			string folder_path = System.IO.Path.Combine( Application.persistentDataPath, folder_name );

			if ( ! System.IO.Directory.Exists( folder_path ) )
			{
				System.IO.Directory.CreateDirectory( folder_path );
			}

			string file_path = System.IO.Path.Combine( folder_path, file_name );

			Debug.Log( $"saving '{file_path}'" );

			Task.Run( ()=> System.IO.File.WriteAllBytes( file_path, blob ) ).ConfigureAwait( false );
		}
		private
		bool
		difference_timer_ended()
		{
			bool ret = false;
			float new_differenceTimerStart = Time.realtimeSinceStartup;
			float d_s = new_differenceTimerStart - differenceTimerStart;
			if ( d_s >= differenceCheckPeriodSecs )
			{
				d_s -= differenceCheckPeriodSecs;
				new_differenceTimerStart -= d_s;
				differenceTimerStart = new_differenceTimerStart;

				ret = true;
			}
			return ret;
		}
		private
		void
		if_different_save_and_advance()
		{
			if ( simDuration > maxTotalDurationSecs || stepsWereDifferent( saveMaxDifferenceThreshold ) )
			{
				saveToPNG();

				parameters.f += fIncrement;
				parameters.k += kIncrement;

				restart();
			}
		}
		#endregion private
	}
}
